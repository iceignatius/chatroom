#include "MessageForm.h"

using namespace std;
using namespace ui;

MessageForm::MessageForm(FormManager *mgr) :
    FormBase(mgr), timer_enabled(false), timer_remain(0)
{
}

const char* MessageForm::KlassName()
{
    return "messageform";
}

Form* MessageForm::CreateInstance(FormManager *mgr)
{
    return new MessageForm(mgr);
}

void MessageForm::OnPush(const FormParams &params)
{
    text = params.GetStringParam("text", "");
    if( !text.length() )
        Close();

    timer_remain = params.GetUintParam("timeout", 0);
    timer_enabled = timer_remain;
}

void MessageForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);
    for(int key = 0; key < KEY_MAX; ++key)
    {
        if( keystate[key] )
            Close();
    }

    if( timer_enabled )
    {
        timer_remain -= min(timer_remain, steptime);
        if( !timer_remain )
            Close();
    }
}

void MessageForm::Render()
{
    Manager()->GetConsole()->RenderMessageBox(text);
}
