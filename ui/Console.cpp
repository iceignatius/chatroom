#include <assert.h>
#include <string.h>
#include <stdexcept>
#include "Console.h"

using namespace std;
using namespace ui;

Console::Console() :
    started(false)
{
}

Console::~Console()
{
    Shutdown();
}

void Console::Setup()
{
    if( started ) return;

    bool lib_inited = false;
    try
    {
        if( !initscr() )
            throw runtime_error("Initialise curses library failed");
        lib_inited = true;

        // Catch most of keys input.
        if( cbreak() )
            throw runtime_error("Set property failed!");

        // No new-line.
        if( nonl() )
            throw runtime_error("Set property failed!");

        // Not show the input characters.
        if( noecho() )
            throw runtime_error("Set property failed!");

        // Catch special keys.
        if( keypad(stdscr, true) )
            throw runtime_error("Set property failed!");

        // Not wait key input if the input buffer is empty.
        if( notimeout(stdscr, true) )
            throw runtime_error("Set property failed!");
        if( nodelay(stdscr, true) )
            throw runtime_error("Set property failed!");

        // Hide cursor.
        curs_set(0);

        started = true;
    }
    catch(exception &e)
    {
        if( lib_inited )
            endwin();
        throw;
    }
}

void Console::Shutdown()
{
    if( !started ) return;

    endwin();
    started = false;
}

void Console::BeginRender()
{
    if( clear() )
        throw runtime_error("Clear screen failed!");
}

void Console::EndRender()
{
    if( refresh() )
        throw runtime_error("Update screen failed!");
}

void Console::RenderHLine(int x, int y, int len, char body, char edge)
{
    for(int i = 1; i < len - 1; ++i)
        mvaddch(y, x + i, body);

    mvaddch(y, x, edge);
    mvaddch(y, x + len - 1, edge);
}

void Console::RenderVLine(int x, int y, int len, char body, char edge)
{
    for(int i = 1; i < len - 1; ++i)
        mvaddch(y + i, x, body);

    mvaddch(y, x, edge);
    mvaddch(y + len - 1, x, edge);
}

void Console::RenderMessageBox(const string &text)
{
    int len = text.length();
    int box_cols = len + 4;
    int box_rows = 3;

    int screen_cols = getmaxx(stdscr);
    int screen_rows = getmaxy(stdscr);
    assert( screen_cols >= box_cols );
    assert( screen_rows >= box_rows );

    int box_x = ( screen_cols - box_cols ) / 2;
    int box_y = ( screen_rows - box_rows ) / 2;

    RenderHLine(box_x, box_y, box_cols, '-', '+');
    RenderHLine(box_x, box_y + 3 - 1, box_cols, '-', '+');

    move(box_y + 1, box_x);
    addstr("| ");
    printw(text.c_str());
    addstr(" |");
}

const bool* Console::GetKeyboardState(bool state[KEY_MAX])
{
    static bool internal_buffer[KEY_MAX];
    bool *state_array = state ? state : internal_buffer;

    memset(state_array, 0, sizeof(state_array[0])*KEY_MAX);
    for(int key; ( key = getch() ) >= 0; )
    {
        assert( key < KEY_MAX );
        state_array[key] = true;
    }

    return state_array;
}
