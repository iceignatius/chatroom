#ifndef _UI_FORM_BASH_H_
#define _UI_FORM_BASH_H_

#include "FormInterface.h"

namespace ui
{

class FormBase : public Form
{
private:
    FormManager *mgr;
    FormState state;
    int exitcode;

    bool kbdstate[KEY_MAX];

public:
    FormBase(FormManager *mgr);

public:
    FormState State() const override;
    int ExitCode() const override;

    void Close(int exitcode = 0) override;
    void Suspend() override;
    void Hide() override;
    void Show() override;

protected:
    FormManager* Manager();

    const bool* GetKeyboardState(bool state[KEY_MAX]);
};

}   // namespace ui

#endif
