#ifndef _CONSOLE_H_
#define _CONSOLE_H_

#include <string>
#include <ncurses.h>

namespace ui
{

class Console
{
private:
    bool started;

public:
    Console();
    ~Console();

public:
    void Setup();
    void Shutdown();

    void BeginRender();
    void EndRender();

    void RenderHLine(int x, int y, int len, char body, char edge);
    void RenderVLine(int x, int y, int len, char body, char edge);
    void RenderMessageBox(const std::string &text);

    const bool* GetKeyboardState(bool state[KEY_MAX]);
};

}   // namespace ui

#endif
