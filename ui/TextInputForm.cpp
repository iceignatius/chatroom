#include "common/ascii.h"
#include "TextInputForm.h"

using namespace std;
using namespace ui;

TextInputForm::TextInputForm(FormManager *mgr) :
    FormBase(mgr), dialog_width(4), dialog_height(6)
{
}

const char* TextInputForm::KlassName()
{
    return "text-input-form";
}

Form* TextInputForm::CreateInstance(FormManager *mgr)
{
    return new TextInputForm(mgr);
}

string TextInputForm::ReplaceControlCodes(
    const string &text,
    char replacement)
{
    string str = text;
    for(size_t i = 0; i < str.length(); ++i)
    {
        if( iscntrl(str[i]) )
            str[i] = replacement;
    }

    return str;
}

list<string> TextInputForm::ParseTextLines(
    const string &text,
    char ctrl_replacement)
{
    list<string> lines;

    string str;
    for(size_t i = 0; i < text.length(); ++i)
    {
        char ch = text[i];
        if( ch == '\n' )
        {
            lines.push_back(ReplaceControlCodes(str, ctrl_replacement));
            str.clear();
        }
        else
        {
            str.push_back(ch);
        }
    }

    if( text.back() != '\n' )
        lines.push_back(str);

    return lines;
}

void TextInputForm::OnPush(const FormParams &params)
{
    title = params.GetStringParam("title", "");
    title = ReplaceControlCodes(title, '*');

    string prompt = params.GetStringParam("prompt", "");
    prompt_lines = ParseTextLines(prompt, '*');

    unsigned max_text_width = title.length() ? title.length() + 2 : 0;
    for(string &str : prompt_lines)
    {
        if( max_text_width < str.length() )
            max_text_width = str.length();
    }

    dialog_width = 2 + max_text_width + 2;
    dialog_height = 2 + prompt_lines.size() + 2 + 2;
}

void TextInputForm::OnPop(FormParams &params)
{
    if( ExitCode() == 0 )
        params.InsertStringParam("result", input);
}

void TextInputForm::PushCharacter(int ch)
{
    if( isprint(ch) )
        input.push_back(ch);

    if( ch == ASCII_BS )
        input.pop_back();

    if( ch == ASCII_DEL )
        input.clear();
}

void TextInputForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);

    // Process work state keys.
    if( keystate[ASCII_ESC] )
        Close(1);
    if( keystate[ASCII_CR] || keystate[KEY_ENTER] )
        Close();

    // Process for ASCII codes.
    for(int ch = 0; ch < 0x80; ++ch)
    {
        if( keystate[ch] )
            PushCharacter(ch);
    }

    // Process input modify keys.

    if( keystate[KEY_BACKSPACE] )
        PushCharacter(ASCII_BS);

    if( keystate[KEY_DC] )
        PushCharacter(ASCII_DEL);
}

void TextInputForm::Render()
{
    int screen_cols = getmaxx(stdscr);
    int screen_rows = getmaxy(stdscr);

    int dialog_posx =
        screen_cols >= dialog_width ?
        ( screen_cols - dialog_width ) / 2 :
        0;
    int dialog_posy =
        screen_rows >= dialog_height ?
        ( screen_rows - dialog_height ) / 2 :
        0;

    Console *console = Manager()->GetConsole();

    console->RenderVLine(
        dialog_posx,
        dialog_posy,
        dialog_height,
        '|',
        '+');
    console->RenderVLine(
        dialog_posx + dialog_width - 1,
        dialog_posy,
        dialog_height,
        '|',
        '+');
    console->RenderHLine(
        dialog_posx,
        dialog_posy,
        dialog_width,
        '-',
        '+');
    console->RenderHLine(
        dialog_posx,
        dialog_posy + prompt_lines.size() + 4 - 1,
        dialog_width,
        '-',
        '+');
    console->RenderHLine(
        dialog_posx,
        dialog_posy + dialog_height - 1,
        dialog_width,
        '-',
        '+');

    if( title.length() )
    {
        int xoff = ( dialog_width - title.length() - 2 ) / 2;
        mvprintw(dialog_posy, dialog_posx + xoff, " %s ", title.c_str());
    }

    if( prompt_lines.size() )
    {
        int yoff = 2;
        for(string &str : prompt_lines)
        {
            int xoff = ( dialog_width - str.length() ) / 2;
            mvprintw(dialog_posy + yoff, dialog_posx + xoff, "%s", str.c_str());
            ++yoff;
        }
    }

    if( input.length() )
    {
        int xoff = ( dialog_width - input.length() ) / 2;
        mvprintw(dialog_posy + dialog_height - 2, dialog_posx + xoff, "%s", input.c_str());
    }
}
