#ifndef _UI_TEXT_INPUT_FORM_H_
#define _UI_TEXT_INPUT_FORM_H_

#include <list>
#include "ui/FormBase.h"

namespace ui
{

class TextInputForm : public ui::FormBase
{
private:
    std::string title;
    std::list<std::string> prompt_lines;
    std::string input;

    int dialog_width;
    int dialog_height;

private:
    TextInputForm(ui::FormManager *mgr);

private:
    static std::string ReplaceControlCodes(
        const std::string &text,
        char replacement);

    static std::list<std::string> ParseTextLines(
        const std::string &text,
        char ctrl_replacement);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    void OnPush(const ui::FormParams &params) override;
    void OnPop(ui::FormParams &params) override;

    void PushCharacter(int ch);

public:
    void Update(unsigned steptime) override;
    void Render() override;
};

}   // namespace ui

#endif
