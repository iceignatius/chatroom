#ifndef _UI_FORM_MANAGER_IMPL_H_
#define _UI_FORM_MANAGER_IMPL_H_

#include <memory>
#include <list>
#include "FormInterface.h"

namespace ui
{

class FormManagerImpl : public FormManager
{
private:
    Console console;

    std::map< std::string, Form*(*)(FormManager*) > form_factory;
    std::list<std::shared_ptr<Form>> form_stack;

    bool terminating;
    int exitcode;

public:
    FormManagerImpl();
    ~FormManagerImpl();

public:
    void Setup();
    void Shutdown();

public:
    Console* GetConsole() override;

    void RegisterFormKlass(
        const std::string &klass,
        Form*(*creator)(FormManager *mgr)) override;

    Form* FocusForm() override;
    void PushNewForm(const std::string &klass, const FormParams &params) override;
    void Terminate(int exitcode) override;

private:
    int RemoveClosedFocusForms(FormParams &params);
    void UpdateForms(unsigned steptime);
    void RenderForms();

public:
    int RunModal(Form *stopform = nullptr, FormParams *params = nullptr) override;
};

}   // namespace ui

#endif
