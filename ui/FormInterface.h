#ifndef _UI_FORM_INTERFACE_H_
#define _UI_FORM_INTERFACE_H_

#include "Console.h"
#include "FormParam.h"

namespace ui
{

enum class FormState
{
    Closed,     // The form will do nothing but wait to be destroyed.
    Suspended,  // The form is freezing, but can be activated again.
    Hidden,     // The form will be updated only.
    Shown,      // The form will be updated and rendered.
    Focus,      // The form will be updated, rendered, and be allowed to get user input.
};

class Form
{
public:
    virtual ~Form() {}

public:
    virtual FormState State() const = 0;
    virtual int ExitCode() const = 0;

    virtual void Close(int exitcode) = 0;
    virtual void Suspend() = 0;
    virtual void Hide() = 0;
    virtual void Show() = 0;

    virtual void Update(unsigned steptime) = 0;
    virtual void Render() = 0;

    virtual void OnPush(const FormParams &setup_params) {}
    virtual void OnPop(FormParams &result_params) {}
    virtual void OnFocus(const FormParams &prev_params) {}
};

class FormManager
{
public:
    virtual Console* GetConsole() = 0;

    virtual void RegisterFormKlass(
        const std::string &klass,
        Form*(*creator)(FormManager *mgr)) = 0;

    virtual Form* FocusForm() = 0;
    virtual void PushNewForm(const std::string &klass, const FormParams &params) = 0;
    virtual void Terminate(int exitcode) = 0;

    virtual int RunModal(Form *stopform = nullptr, FormParams *params = nullptr) = 0;
};

}   // namespace ui

#endif
