#ifndef _TIME_UTILITY_H_
#define _TIME_UTILITY_H_

#ifdef __cplusplus
extern "C" {
#endif

void tmutl_sleep(unsigned ms);
unsigned tmutl_get_mstime(void);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
