#include <assert.h>
#include <time.h>

#ifdef __linux__
#   include <unistd.h>
#endif

#ifdef _WIN32
#   include <windows.h>
#endif

#include "tmutl.h"

void tmutl_sleep(unsigned ms)
{
#if defined(__linux__)
    static const unsigned ms_max = ((unsigned)-1) / 1000;
    usleep( 1000 * (( ms > ms_max )?( ms_max ):( ms )) );
#elif defined(_WIN32)
    Sleep(ms);
#else
    #error No implementation on this platform!
#endif
}

unsigned tmutl_get_mstime(void)
{
#if defined(__linux__)

#   if   defined(CLOCK_MONOTONIC_RAW)
    static const clockid_t clockid = CLOCK_MONOTONIC_RAW;
#   elif defined(CLOCK_MONOTONIC)
    static const clockid_t clockid = CLOCK_MONOTONIC;
#   else
    static const clockid_t clockid = CLOCK_REALTIME;
#   endif

    struct timespec tp;
    int gettime_result = clock_gettime(clockid, &tp);
    assert( gettime_result == 0 );
    (void)gettime_result;   // Avoid the unused warning!

    return tp.tv_sec * 1000 + tp.tv_nsec / (1000*1000);

#elif defined(_WIN32)

    return GetTickCount();

#else
    #error No implementation on this platform!
#endif
}
