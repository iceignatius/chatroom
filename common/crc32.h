#ifndef _CRC32_H_
#define _CRC32_H_

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint32_t crc32(const void *data, size_t size);

#ifdef __cplusplus
}  // extern "C"
#endif

#endif
