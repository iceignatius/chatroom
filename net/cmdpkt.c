#include <assert.h>
#include <string.h>
#include <endian.h>
#include "common/crc32.h"
#include "cmdcrypt.h"
#include "cmdpkt.h"

#pragma pack(push, 1)
struct header
{
    uint64_t    signature;
    uint16_t    sender;
    uint16_t    flags;
    uint64_t    cmdid;
    uint16_t    cmdsn;
    uint16_t    argslen;
    uint8_t     argsdata[];
};
#pragma pack(pop)

#pragma pack(push, 1)
struct trailer
{
    uint8_t     mac[16];
    uint32_t    crc;
};
#pragma pack(pop)

#define PKTSIGN 0x3EBAF03FA11957B6
#define PKTSIZE(argslen) ( sizeof(struct header) + (argslen) + sizeof(struct trailer) )
#define MIN_PKTSIZE PKTSIZE(0)

#define PKT_TRAILER(pkt) \
    ( \
        (struct trailer*)\
        ( \
            ((uint8_t*)pkt) + \
            sizeof(struct header) + \
            be16toh( ((struct header*)pkt)->argslen ) \
        ) \
    )

size_t cmdpkt_init(void *buf, size_t bufsize)
{
    if( !buf || !bufsize ) return MIN_PKTSIZE;
    if( bufsize < MIN_PKTSIZE ) return 0;

    memset(buf, 0, MIN_PKTSIZE);

    struct header *hdr = buf;
    hdr->signature  = htobe64(PKTSIGN);
    hdr->sender     = htobe16(CMDSENDER_ANON);

    return MIN_PKTSIZE;
}

size_t cmdpkt_calc_pktsize(size_t argslen)
{
    return PKTSIZE(argslen);
}

size_t cmdpkt_get_pktsize(const void *pkt)
{
    const struct header *hdr = pkt;
    return pkt ? PKTSIZE(be16toh(hdr->argslen)) : 0;
}

void cmdpkt_set_sender(void *pkt, int sender)
{
    struct header *hdr = pkt;
    hdr->sender = htobe16(sender);
}

void cmdpkt_set_flags(void *pkt, unsigned flags)
{
    struct header *hdr = pkt;
    hdr->flags = htobe16(flags);
}

void cmdpkt_set_cmdid(void *pkt, cmdid_t id)
{
    struct header *hdr = pkt;
    hdr->cmdid = htobe64(id);
}

void cmdpkt_set_cmdsn(void *pkt, uint16_t sn)
{
    struct header *hdr = pkt;
    hdr->cmdsn = htobe16(sn);
}

size_t cmdpkt_set_args(
    void        *pkt,
    size_t      max_pkt_size,
    const void  *argsdata,
    size_t      argslen)
{
    if( !pkt || !max_pkt_size ) return PKTSIZE(argslen);
    if( argslen > UINT16_MAX ) return 0;
    if( max_pkt_size < PKTSIZE(argslen) ) return 0;

    struct header *hdr = pkt;
    hdr->argslen = htobe16(argslen);
    memcpy(hdr->argsdata, argsdata, argslen);

    return PKTSIZE(argslen);
}

int cmdpkt_get_sender(const void *pkt)
{
    const struct header *hdr = pkt;
    return (int16_t) be16toh(hdr->sender);
}

unsigned cmdpkt_get_flags(const void *pkt)
{
    const struct header *hdr = pkt;
    return be16toh(hdr->flags);
}

cmdid_t cmdpkt_get_cmdid(const void *pkt)
{
    const struct header *hdr = pkt;
    return be64toh(hdr->cmdid);
}

uint16_t cmdpkt_get_cmdsn(const void *pkt)
{
    const struct header *hdr = pkt;
    return be16toh(hdr->cmdsn);
}

size_t cmdpkt_get_argslen(const void *pkt)
{
    const struct header *hdr = pkt;
    return be16toh(hdr->argslen);
}

const void* cmdpkt_get_argsdata(const void *pkt)
{
    const struct header *hdr = pkt;
    return hdr->argsdata;
}

uint32_t cmdpkt_get_crc(const void *pkt)
{
    const struct trailer *trl = PKT_TRAILER(pkt);
    return be32toh(trl->crc);
}

void cmdpkt_encode_crc(void *pkt)
{
    struct header *hdr = pkt;
    struct trailer *trl = PKT_TRAILER(pkt);

    const uint8_t *startpos = (const uint8_t*) hdr;
    const uint8_t *endpos = (const uint8_t*) &trl->crc;
    size_t size = endpos - startpos;
    uint32_t crcval = crc32(startpos, size);

    trl->crc = htobe32(crcval);
}

void cmdpkt_encode_mac(void *pkt, const void *key, size_t keysize)
{
    struct trailer *trl = PKT_TRAILER(pkt);

    uint8_t macdata[16] = {0};
    assert( sizeof(macdata) == CMDCRYPT_BLOCK_SIZE );

    if( key && keysize >= CMDCRYPT_SKEY_SIZE )
    {
        const void *srcdata = pkt;
        size_t srcsize = cmdpkt_get_pktsize(pkt) - sizeof(*trl);
        cmdcrypt_calc_mac(macdata, srcdata, srcsize, key);
    }

    memcpy(trl->mac, macdata, sizeof(trl->mac));
}

int cmdpkt_verify_format(const void *pkt, size_t pktsize)
{
    /*
     * @retval 0 The input data is a completed packet and the format is good.
     * @retval Positive The return value indicates
     *                  how many octets will be needed to complete the packet.
     * @retval Negative The input data is not a valid packet.
     */
    if( !pkt ) return -1;
    if( pktsize < MIN_PKTSIZE ) return MIN_PKTSIZE - pktsize;

    const struct header *hdr = pkt;
    if( be64toh(hdr->signature) != PKTSIGN ) return -1;

    size_t tgtsize = PKTSIZE(be16toh(hdr->argslen));
    if( pktsize < tgtsize ) return tgtsize - pktsize;

    return 0;
}

int cmdpkt_verify_crc(const void *pkt)
{
    const struct header *hdr = pkt;
    const struct trailer *trl = PKT_TRAILER(pkt);

    const uint8_t *startpos = (const uint8_t*) hdr;
    const uint8_t *endpos = (const uint8_t*) &trl->crc;
    size_t size = endpos - startpos;
    uint32_t crcval = crc32(startpos, size);

    return be32toh(trl->crc) == crcval ? 0 : -1;
}

int cmdpkt_verify_mac(const void *pkt, const void *key, size_t keysize)
{
    const struct trailer *trl = PKT_TRAILER(pkt);

    uint8_t macdata[16] = {0};
    assert( sizeof(macdata) == CMDCRYPT_BLOCK_SIZE );

    if( keysize >= CMDCRYPT_SKEY_SIZE )
    {
        const void *srcdata = pkt;
        size_t srcsize = cmdpkt_get_pktsize(pkt) - sizeof(*trl);
        cmdcrypt_calc_mac(macdata, srcdata, srcsize, key);
    }

    return 0 == memcmp(trl->mac, macdata, sizeof(trl->mac)) ? 0 : -1;
}
