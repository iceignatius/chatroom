#ifndef _CMD_PEER_BASE_H_
#define _CMD_PEER_BASE_H_

#include <memory>
#include <map>
#include <netsock/sockudp.h>
#include "CmdPeer.h"

namespace net
{

class CmdPeerBase : public CmdPeer
{
private:
    const int userid;
    SocketUdp *sock;
    SocketAddr remote_addr;
    std::vector<uint8_t> skey;

    std::map< cmdid_t, std::shared_ptr<CmdProcessor> > proclist;

    bool activated;
    unsigned last_send_time;
    unsigned last_recv_time;

public:
    CmdPeerBase(
        int userid,
        SocketUdp *sock,
        const std::vector<uint8_t> &skey = std::vector<uint8_t>());

public:
    virtual int UserId() const override { return userid; }

    virtual void AddProcessor(CmdProcessor *processor) override;
    virtual void RemoveProcessor(CmdProcessor *processor) override;

    virtual void Active(const SocketAddr &addr) override;
    virtual void Inactive() override;
    virtual bool IsActivated() const override;

    virtual std::vector<uint8_t> EncodePacket(const CmdMsg &msg) const override;
    virtual bool Send(const void *pkt, const SocketAddr *dstaddr) override;
    virtual void OnReceive(const void *pkt, const SocketAddr &srcaddr) override;
    virtual void OnConnectionError() override;

private:
    void SendRejectMessageResponse(
        const void *reqpkt,
        const SocketAddr &dstaddr,
        int err);

public:
    virtual unsigned LastSendTime() const override;
    virtual unsigned LastReceiveTime() const override;

    virtual void RunStep(unsigned steptime) override;
};

}   // namespace net

#endif
