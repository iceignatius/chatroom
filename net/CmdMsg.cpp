#include <string.h>
#include "cmdpkt.h"
#include "CmdMsg.h"

using namespace std;
using namespace net;

CmdMsg::CmdMsg(cmdid_t cmd) :
    sender(CMDSENDER_ANON),
    flags(0),
    cmd(cmd),
    sn(0)
{
    if( !( args = ndobj_create_map("") ) )
        throw bad_alloc();
}

CmdMsg::CmdMsg(const CmdMsg &src) :
    sender(src.sender),
    flags(src.flags),
    cmd(src.cmd),
    sn(src.sn)
{
    args = ndobj_ele_newref(src.args);
}

CmdMsg::~CmdMsg()
{
    if( args )
        ndobj_ele_release(args);
}

CmdMsg& CmdMsg::operator =(const CmdMsg &src)
{
    this->sender = src.sender;
    this->flags = src.flags;
    this->cmd = src.cmd;
    this->sn = src.sn;

    ndobj_ele_release(this->args);
    this->args = ndobj_ele_newref(src.args);

    return *this;
}

bool CmdMsg::operator ==(const CmdMsg &other) const
{
    if( cmd == 0 && other.cmd == 0 )
        return true;

    return
        sender == other.sender &&
        flags == other.flags &&
        cmd == other.cmd &&
        sn == other.sn &&
        args == other.args;
}

bool CmdMsg::operator !=(const CmdMsg &other) const
{
    if( cmd == 0 && other.cmd == 0 )
        return false;

    return
        sender != other.sender ||
        flags != other.flags ||
        cmd != other.cmd ||
        sn != other.sn ||
        args != other.args;
}

size_t CmdMsg::Encode(
    void *buf,
    size_t bufsize,
    const uint8_t *skey,
    size_t keysize) const
{
    if( !skey )
        keysize = 0;

    void *pkt = buf;
    if( !cmdpkt_init(pkt, bufsize) ) return 0;

    cmdpkt_set_sender(pkt, sender);
    cmdpkt_set_flags(pkt, flags | ( keysize ? 0 : CMDFLAG_NO_MAC ));
    cmdpkt_set_cmdid(pkt, cmd);
    cmdpkt_set_cmdsn(pkt, sn);

    vector<uint8_t> rawargs;
    if( ndobj_ele_count_children(args) )
    {
        rawargs.resize(bufsize);

        size_t rawsize = ndobj_ele_encode(args, rawargs.data(), rawargs.size());
        if( !rawsize ) return 0;

        rawargs.resize(rawsize);
    }

    size_t pktsize = cmdpkt_set_args(pkt, bufsize, rawargs.data(), rawargs.size());
    if( !pktsize ) return 0;

    cmdpkt_encode_mac(pkt, skey, keysize);
    cmdpkt_encode_crc(pkt);

    return pktsize;
}

vector<uint8_t> CmdMsg::Encode(const uint8_t *skey, size_t keysize) const
{
    size_t args_size =
        ndobj_ele_count_children(args) ?
        ndobj_ele_encode(args, nullptr, 0) :
        0;

    vector<uint8_t> pkt(cmdpkt_calc_pktsize(args_size));
    size_t pktsize = Encode(pkt.data(), pkt.size(), skey, keysize);
    pkt.resize(pktsize);

    return pkt;
}

void CmdMsg::Decode(const void *pkt)
{
    sender = cmdpkt_get_sender(pkt);
    flags = cmdpkt_get_flags(pkt);
    cmd = cmdpkt_get_cmdid(pkt);
    sn = cmdpkt_get_cmdsn(pkt);

    const void *rawdata = cmdpkt_get_argsdata(pkt);
    size_t rawsize = cmdpkt_get_argslen(pkt);

    ndobj_ele_t *newargs = rawsize ? ndobj_parse(rawdata, rawsize) : nullptr;
    if( newargs )
    {
        ndobj_ele_release(args);
        args = newargs;
    }
}

CmdMsg CmdMsg::FromPacket(const void *pkt)
{
    CmdMsg msg(0);
    msg.Decode(pkt);
    return msg;
}

vector<uint8_t> CmdMsg::GetBinaryArg(const string &name) const
{
    vector<uint8_t> value;

    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, name.c_str());
    if( !child || !ndobj_ele_is_binary(child) )
        return value;

    const void *data = ndobj_ele_get_bindata(child);
    size_t size = ndobj_ele_get_binsize(child);

    value.resize(size);
    memcpy(value.data(), data, size);
    return value;
}

string CmdMsg::GetStringArg(const string &name) const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, name.c_str());
    if( !child || !ndobj_ele_is_string(child) )
        return "";

    return ndobj_ele_get_string(child);
}

int CmdMsg::GetIntegerArg(const string &name, int failval) const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, name.c_str());
    if( !child || !ndobj_ele_is_integer(child) )
        return failval;

    return ndobj_ele_get_integer(child);
}

unsigned CmdMsg::GetUnsignedArg(const string &name, unsigned failval) const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, name.c_str());
    if( !child || !ndobj_ele_is_unsigned(child) )
        return failval;

    return ndobj_ele_get_unsigned(child);
}

bool CmdMsg::GetBooleanArg(const string &name, bool failval) const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, name.c_str());
    if( !child || !ndobj_ele_is_boolean(child) )
        return failval;

    return ndobj_ele_get_boolean(child);
}

double CmdMsg::GetFloatArg(const string &name, double failval) const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, name.c_str());
    if( !child || !ndobj_ele_is_float(child) )
        return failval;

    return ndobj_ele_get_float(child);
}

int CmdMsg::GetErrorCode() const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, "error-code");
    if( !child || !ndobj_ele_is_integer(child) )
        return -1;

    return ndobj_ele_get_integer(child);
}

string CmdMsg::GetErrorText() const
{
    const ndobj_ele_t *child = ndobj_ele_get_first_child(args, "error-text");
    if( !child || !ndobj_ele_is_string(child) )
        return "";

    return ndobj_ele_get_string(child);
}

void CmdMsg::AddBinaryArg(const string &name, const void *data, size_t size)
{
    ndobj_ele_t *child = ndobj_create_binary(name.c_str(), data, size);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}

void CmdMsg::AddBinaryArg(const string &name, const vector<uint8_t> &data)
{
    AddBinaryArg(name, data.data(), data.size());
}

void CmdMsg::AddStringArg(const string &name, const string &value)
{
    ndobj_ele_t *child = ndobj_create_string(name.c_str(), value.c_str());
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}

void CmdMsg::AddIntegerArg(const string &name, int value)
{
    ndobj_ele_t *child = ndobj_create_integer(name.c_str(), value);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}

void CmdMsg::AddUnsignedArg(const string &name, unsigned value)
{
    ndobj_ele_t *child = ndobj_create_unsigned(name.c_str(), value);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}

void CmdMsg::AddBooleanArg(const string &name, bool value)
{
    ndobj_ele_t *child = ndobj_create_boolean(name.c_str(), value);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}

void CmdMsg::AddFloatArg(const string &name, double value)
{
    ndobj_ele_t *child = ndobj_create_float(name.c_str(), value);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}

void CmdMsg::AddErrorCode(int errcode)
{
    ndobj_ele_t *child = ndobj_create_integer("error-code", errcode);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }

    const char *errtext = errcode ? cmdrc_to_str(errcode) : "";
    child = ndobj_create_string("error-text", errtext);
    if( !child ) throw bad_alloc();

    if( !ndobj_ele_add_child(args, child) )
    {
        ndobj_ele_release(child);
        throw bad_alloc();
    }
}
