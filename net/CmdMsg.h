#ifndef _CMD_MSG_H_
#define _CMD_MSG_H_

#include <string>
#include <vector>
#include <ndobj/ndobj.h>
#include "cmddef.h"

namespace net
{

class CmdMsg
{
public:
    int         sender;
    unsigned    flags;
    cmdid_t     cmd;
    uint16_t    sn;
    ndobj_ele_t *args;

public:
    CmdMsg(cmdid_t cmd);
    CmdMsg(const CmdMsg &src);
    ~CmdMsg();

public:
    CmdMsg& operator =(const CmdMsg &src);

    bool operator ==(const CmdMsg &other) const;
    bool operator !=(const CmdMsg &other) const;

public:
    bool IsRequestMsg() const { return !( sn & 1 ); }
    bool IsResponseMsg() const { return sn & 1; }

public:
    size_t Encode(
        void *buf,
        size_t bufsize,
        const uint8_t *skey,
        size_t keysize) const;
    std::vector<uint8_t> Encode(const uint8_t *skey, size_t keysize) const;
    void Decode(const void *pkt);

    static CmdMsg FromPacket(const void *pkt);

public:
    std::vector<uint8_t> GetBinaryArg(const std::string &name) const;
    std::string GetStringArg(const std::string &name) const;
    int GetIntegerArg(const std::string &name, int failval) const;
    unsigned GetUnsignedArg(const std::string &name, unsigned failval) const;
    bool GetBooleanArg(const std::string &name, bool failval) const;
    double GetFloatArg(const std::string &name, double failval) const;
    int GetErrorCode() const;
    std::string GetErrorText() const;

    void AddBinaryArg(const std::string &name, const void *data, size_t size);
    void AddBinaryArg(const std::string &name, const std::vector<uint8_t> &data);
    void AddStringArg(const std::string &name, const std::string &value);
    void AddIntegerArg(const std::string &name, int value);
    void AddUnsignedArg(const std::string &name, unsigned value);
    void AddBooleanArg(const std::string &name, bool value);
    void AddFloatArg(const std::string &name, double value);
    void AddErrorCode(int errcode = CMDRC_SUCCESS);
};

}   // namespace net

#endif
