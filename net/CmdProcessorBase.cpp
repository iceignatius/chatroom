#include "common/tmutl.h"
#include "netdef.h"
#include "cmdpkt.h"
#include "CmdProcessorBase.h"

using namespace std;
using namespace net;

CmdProcessorBase::CmdProcessorBase(cmdid_t cmdid, int flags, CmdPeer *peer) :
    cmdid(cmdid),
    flags(flags),
    peer(peer),
    next_send_sn(0),
    next_recv_sn(0)
{
}

vector<uint8_t> CmdProcessorBase::FindOldResponsePacket(const void *reqpkt)
{
    uint16_t req_sn = cmdpkt_get_cmdsn(reqpkt);
    uint32_t req_crc = cmdpkt_get_crc(reqpkt);

    for(auto info : old_resp_list)
    {
        if( info->req_sn == req_sn && info->req_crc == req_crc )
            return info->resp_pkt;
    }

    return vector<uint8_t>();
}

void CmdProcessorBase::ProcRequestMessage(const void *pkt, const SocketAddr &srcaddr)
{
    if( flags & AUTO_RESP )
    {
        vector<uint8_t> resp_pkt = FindOldResponsePacket(pkt);
        if( resp_pkt.size() )
        {
            peer->Send(resp_pkt.data());
            return;
        }
    }

    uint16_t sn = cmdpkt_get_cmdsn(pkt);
    if( cmdpkt_get_flags(pkt) & CMDFLAG_NO_RESP )
    {
        uint16_t diff = sn - next_recv_sn;
        if( diff >= UINT16_MAX / 2 )
            return;
    }
    else
    {
        if( sn != next_recv_sn )
            return;
    }

    if( flags & ACCUMULATE_SN )
        next_recv_sn = sn + 2;

    OnRequestMessage(CmdMsg::FromPacket(pkt), cmdpkt_get_crc(pkt), srcaddr);
}

void CmdProcessorBase::ProcResponseMessage(const void *pkt, const SocketAddr &srcaddr)
{
    if( !( flags & ALLOW_UNMANAGED_RESP ) )
    {
        if( !curr_req.avail ) return;

        uint16_t resp_sn = curr_req.sn + 1;
        if( cmdpkt_get_cmdsn(pkt) != resp_sn ) return;
    }

    OnResponseMessage(CmdMsg::FromPacket(pkt), srcaddr);
    curr_req.avail = false;
}

void CmdProcessorBase::CheckSendRequestMessage()
{
    // Check resend the current request.
    do
    {
        if( !curr_req.avail ) break;

        unsigned currtime = tmutl_get_mstime();
        unsigned timepass = currtime - curr_req.sendtime;
        if( timepass <= NETDEF_RESEND_TIMEOUT ) break;

        if( !curr_req.trycount )
        {
            peer->OnConnectionError();
            curr_req.avail = false;
            break;
        }

        if( !peer->Send(curr_req.pkt.data()) )
            break;

        -- curr_req.trycount;
        curr_req.sendtime = currtime;

    } while(false);

    // Check send new request.
    do
    {
        if( curr_req.avail ) break;
        if( !req_list.size() ) break;

        CmdMsg msg = req_list.front();
        req_list.pop_front();

        curr_req.pkt = peer->EncodePacket(msg);
        if( !curr_req.pkt.size() ) break;

        curr_req.avail = true;
        curr_req.sn = msg.sn;
        curr_req.sendtime = tmutl_get_mstime();
        curr_req.trycount = NETDEF_RESEND_TRYCOUNT;

        peer->Send(curr_req.pkt.data());

        if( msg.flags & CMDFLAG_NO_RESP )
            curr_req.avail = false;

    } while(false);
}

bool CmdProcessorBase::SendUnmanagedMessage(const CmdMsg &msg, const SocketAddr &dstaddr)
{
    vector<uint8_t> pkt = peer->EncodePacket(msg);
    if( !pkt.size() ) return false;

    return peer->Send(pkt.data(), &dstaddr);
}

bool CmdProcessorBase::SendRequestMessage(CmdMsg &msg)
{
    msg.sender = peer->UserId();
    msg.sn = next_send_sn;

    if( flags & ACCUMULATE_SN )
        next_send_sn += 2;

    req_list.push_back(msg);
    CheckSendRequestMessage();

    return true;
}

bool CmdProcessorBase::SendResponseMessage(
    CmdMsg &msg,
    uint16_t req_sn,
    uint32_t req_crc)
{
    msg.sender = peer->UserId();
    msg.sn = req_sn + 1;
    if( !msg.IsResponseMsg() ) return false;

    vector<uint8_t> pkt = peer->EncodePacket(msg);
    if( !pkt.size() ) return false;

    auto info = shared_ptr<ResponseInfo>(new ResponseInfo(req_sn, req_crc, pkt));
    old_resp_list.push_back(info);

    return peer->Send(pkt.data());
}

void CmdProcessorBase::OnReceive(const void *pkt, const SocketAddr &srcaddr)
{
    if( cmdpkt_get_cmdsn(pkt) & 0x1 )
        ProcResponseMessage(pkt, srcaddr);
    else
        ProcRequestMessage(pkt, srcaddr);
}

void CmdProcessorBase::RunStep(unsigned steptime)
{
    while( old_resp_list.size() > NETDEF_MAX_OLD_RESP_NUM )
        old_resp_list.pop_front();

    CheckSendRequestMessage();
}
