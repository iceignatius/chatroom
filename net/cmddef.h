#ifndef _CMD_DEF_H_
#define _CMD_DEF_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

enum cmdsender
{
    CMDSENDER_ANON      = -1,
    CMDSENDER_SERVER    = 0,
};

enum cmdflags
{
    CMDFLAG_NO_RESP = 0x1,
    CMDFLAG_NO_MAC  = 0x2,
};

typedef uint64_t cmdid_t;

#define CMDID_FROM_CHARS(c1,c2,c3,c4,c5,c6,c7,c8) \
    ( \
        ( ( ((cmdid_t)(c1)) & 0xFF ) << (7*8) ) | \
        ( ( ((cmdid_t)(c2)) & 0xFF ) << (6*8) ) | \
        ( ( ((cmdid_t)(c3)) & 0xFF ) << (5*8) ) | \
        ( ( ((cmdid_t)(c4)) & 0xFF ) << (4*8) ) | \
        ( ( ((cmdid_t)(c5)) & 0xFF ) << (3*8) ) | \
        ( ( ((cmdid_t)(c6)) & 0xFF ) << (2*8) ) | \
        ( ( ((cmdid_t)(c7)) & 0xFF ) << (1*8) ) | \
        ( ( ((cmdid_t)(c8)) & 0xFF ) << (0*8) ) \
    )

#define CMDID_QUERY CMDID_FROM_CHARS('q','u','e','r','y',0,0,0)
#define CMDID_JOIN  CMDID_FROM_CHARS('j','o','i','n',0,0,0,0)
#define CMDID_QUIT  CMDID_FROM_CHARS('q','u','i','t',0,0,0,0)
#define CMDID_ALIVE CMDID_FROM_CHARS('a','l','i','v','e',0,0,0)
#define CMDID_TALK  CMDID_FROM_CHARS('t','a','l','k',0,0,0,0)

enum cmd_response_code
{
    CMDRC_SUCCESS = 0,

    CMDRC_ERR_AUTH_REQUIRED,
    CMDRC_ERR_PASSPHRASE,
    CMDRC_ERR_JOIN_FULL,
    CMDRC_ERR_UNSUPPORTED_CMD,
};

static inline
const char* cmdrc_to_str(int err)
{
    switch(err)
    {
    case CMDRC_SUCCESS: return "Success";

    case CMDRC_ERR_AUTH_REQUIRED: return "Authentication required!";
    case CMDRC_ERR_PASSPHRASE: return "Password incorrect!";
    case CMDRC_ERR_JOIN_FULL: return "Server is full up to allow more client!";

    default:
        return "Unknown error occurred!";
    }
}

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
