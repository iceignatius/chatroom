#ifndef _CMD_EXCHANGER_IMPL_H_
#define _CMD_EXCHANGER_IMPL_H_

#include <memory>
#include <map>
#include "CmdExchanger.h"

namespace net
{

class CmdExchangerImpl : public CmdExchanger
{
private:
    SocketUdp sock;
    std::map< int, std::shared_ptr<CmdPeer> > peerlist;

public:
    void Open(unsigned port, bool enable_broadcast = false);
    void Close();

    virtual SocketUdp* GetSocket() override;

public:
    virtual void AddPeer(CmdPeer *peer) override;
    virtual void RemovePeer(int userid) override;
    virtual void RemoveAllPeers() override;

public:
    unsigned ReceiveDispatch() override;
    virtual void RunStep(unsigned steptime) override;
};

}   // namespace net

#endif
