#ifndef _CMD_PEER_H_
#define _CMD_PEER_H_

#include "CmdProcessor.h"

namespace net
{

class CmdPeer
{
public:
    virtual ~CmdPeer() {}

public:
    virtual int UserId() const = 0;

    virtual void AddProcessor(CmdProcessor *processor) = 0;
    virtual void RemoveProcessor(CmdProcessor *processor) = 0;

    virtual void Active(const SocketAddr &addr) = 0;
    virtual void Inactive() = 0;
    virtual bool IsActivated() const = 0;

    virtual std::vector<uint8_t> EncodePacket(const CmdMsg &msg) const = 0;
    virtual bool Send(const void *pkt, const SocketAddr *dstaddr = nullptr) = 0;
    virtual void OnReceive(const void *pkt, const SocketAddr &srcaddr) = 0;
    virtual void OnConnectionError() = 0;

    virtual unsigned LastSendTime() const = 0;
    virtual unsigned LastReceiveTime() const = 0;

    virtual void RunStep(unsigned steptime) = 0;
};

}   // namespace net

#endif
