#ifndef _CMD_PROCESSOR_H_
#define _CMD_PROCESSOR_H_

#include <netsock/sockbase.h>
#include "CmdMsg.h"

namespace net
{

class CmdProcessor
{
public:
    virtual ~CmdProcessor() {}

public:
    virtual cmdid_t CmdId() const = 0;

    virtual void OnReceive(const void *pkt, const SocketAddr &srcaddr) = 0;

    virtual void RunStep(unsigned steptime) = 0;
};

}   // namespace net

#endif
