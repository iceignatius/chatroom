#ifndef _CMDPKT_H_
#define _CMDPKT_H_

#include <stddef.h>
#include "cmddef.h"

#ifdef __cplusplus
extern "C" {
#endif

size_t cmdpkt_init(void *buf, size_t bufsize);

size_t cmdpkt_calc_pktsize(size_t argslen);
size_t cmdpkt_get_pktsize(const void *pkt);

void cmdpkt_set_sender(void *pkt, int sender);
void cmdpkt_set_flags(void *pkt, unsigned flags);
void cmdpkt_set_cmdid(void *pkt, cmdid_t id);
void cmdpkt_set_cmdsn(void *pkt, uint16_t sn);

size_t cmdpkt_set_args(
    void        *pkt,
    size_t      max_pkt_size,
    const void  *argsdata,
    size_t      argslen);

int         cmdpkt_get_sender(const void *pkt);
unsigned    cmdpkt_get_flags(const void *pkt);
cmdid_t     cmdpkt_get_cmdid(const void *pkt);
uint16_t    cmdpkt_get_cmdsn(const void *pkt);
size_t      cmdpkt_get_argslen(const void *pkt);
const void* cmdpkt_get_argsdata(const void *pkt);
uint32_t    cmdpkt_get_crc(const void *pkt);

void cmdpkt_encode_crc(void *pkt);
void cmdpkt_encode_mac(void *pkt, const void *key, size_t keysize);

int cmdpkt_verify_format(const void *pkt, size_t pktsize);
int cmdpkt_verify_crc(const void *pkt);
int cmdpkt_verify_mac(const void *pkt, const void *key, size_t keysize);

#ifdef __cplusplus
}   // extern "C"
#endif

#endif
