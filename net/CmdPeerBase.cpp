#include "common/tmutl.h"
#include "net/cmdpkt.h"
#include "CmdPeerBase.h"

using namespace std;
using namespace net;

CmdPeerBase::CmdPeerBase(
    int userid,
    SocketUdp *sock,
    const vector<uint8_t> &skey) :
        userid(userid),
        sock(sock),
        skey(skey),
        activated(false)
{
    last_send_time = last_recv_time = tmutl_get_mstime();
}

void CmdPeerBase::AddProcessor(CmdProcessor *processor)
{
    proclist[processor->CmdId()] = shared_ptr<CmdProcessor>(processor);
}

void CmdPeerBase::RemoveProcessor(CmdProcessor *processor)
{
    proclist.erase(processor->CmdId());
}

void CmdPeerBase::Active(const SocketAddr &addr)
{
    remote_addr = addr;
    activated = true;
}

void CmdPeerBase::Inactive()
{
    activated = false;
}

bool CmdPeerBase::IsActivated() const
{
    return activated;
}

vector<uint8_t> CmdPeerBase::EncodePacket(const CmdMsg &msg) const
{
    return msg.Encode(skey.data(), skey.size());
}

bool CmdPeerBase::Send(const void *pkt, const SocketAddr *dstaddr)
{
    if( !activated && !dstaddr ) return false;

    size_t pktsize = cmdpkt_get_pktsize(pkt);
    if( !pktsize ) return false;

    dstaddr = dstaddr ? dstaddr : &remote_addr;
    int sentsize = sock->SendTo(pkt, pktsize, *dstaddr);
    if( sentsize != (int) pktsize ) return false;

    last_send_time = tmutl_get_mstime();
    return true;
}

void CmdPeerBase::OnReceive(const void *pkt, const SocketAddr &srcaddr)
{
    if( !( cmdpkt_get_flags(pkt) & CMDFLAG_NO_MAC ) )
    {
        if( !skey.size() || cmdpkt_verify_mac(pkt, skey.data(), skey.size()) )
            return;
    }

    last_recv_time = tmutl_get_mstime();

    cmdid_t cmdid = cmdpkt_get_cmdid(pkt);
    auto iter = proclist.find(cmdid);
    if( iter != proclist.end() )
        iter->second->OnReceive(pkt, srcaddr);
    else
        SendRejectMessageResponse(pkt, srcaddr, CMDRC_ERR_UNSUPPORTED_CMD);
}

void CmdPeerBase::OnConnectionError()
{
    Inactive();
}

void CmdPeerBase::SendRejectMessageResponse(
    const void *reqpkt,
    const SocketAddr &dstaddr,
    int err)
{
    CmdMsg respmsg(cmdpkt_get_cmdid(reqpkt));
    respmsg.sn = cmdpkt_get_cmdsn(reqpkt) + 1;
    respmsg.AddErrorCode(err);

    vector<uint8_t> resppkt = EncodePacket(respmsg);
    Send(resppkt.data(), &dstaddr);
}

unsigned CmdPeerBase::LastSendTime() const
{
    return last_send_time;
}

unsigned CmdPeerBase::LastReceiveTime() const
{
    return last_recv_time;
}

void CmdPeerBase::RunStep(unsigned steptime)
{
    for(auto iter : proclist)
        iter.second->RunStep(steptime);
}
