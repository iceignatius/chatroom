#ifndef _CMD_PROCESSOR_BASE_H_
#define _CMD_PROCESSOR_BASE_H_

#include <memory>
#include <list>
#include "CmdPeer.h"
#include "CmdProcessor.h"

namespace net
{

class CmdProcessorBase : public CmdProcessor
{
public:
    enum Flags
    {
        AUTO_RESP = 1 << 0,
        ACCUMULATE_SN = 1 << 1,
        ALLOW_UNMANAGED_RESP = 1 << 2,
    };

private:
    class RequestInfo
    {
    public:
        bool avail;
        uint16_t sn;
        unsigned sendtime;
        unsigned trycount;
        std::vector<uint8_t> pkt;
    public:
        RequestInfo() :
            avail(false), sn(0), sendtime(0), trycount(0)
        {}
    };

    class ResponseInfo
    {
    public:
        uint16_t req_sn;
        uint32_t req_crc;
        std::vector<uint8_t> resp_pkt;
    public:
        ResponseInfo(uint16_t req_sn, uint32_t req_crc, const std::vector<uint8_t> &resp_pkt) :
            req_sn(req_sn), req_crc(req_crc), resp_pkt(resp_pkt)
        {}
    };

private:
    const cmdid_t cmdid;
    int flags;
    CmdPeer *peer;

    uint16_t next_send_sn;
    uint16_t next_recv_sn;

    std::list<CmdMsg> req_list;
    RequestInfo curr_req;

    std::list<std::shared_ptr<ResponseInfo>> old_resp_list;

public:
    CmdProcessorBase(cmdid_t cmdid, int flags, CmdPeer *peer);

public:
    virtual cmdid_t CmdId() const { return cmdid; };

private:
    std::vector<uint8_t> FindOldResponsePacket(const void *reqpkt);

    void ProcRequestMessage(const void *pkt, const SocketAddr &srcaddr);
    void ProcResponseMessage(const void *pkt, const SocketAddr &srcaddr);

protected:
    virtual void OnRequestMessage(
        const CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr)
    {
    }

    virtual void OnResponseMessage(
        const CmdMsg &msg,
        const SocketAddr &srcaddr)
    {
    }

private:
    void CheckSendRequestMessage();

protected:
    bool SendUnmanagedMessage(const CmdMsg &msg, const SocketAddr &dstaddr);
    bool SendRequestMessage(CmdMsg &msg);
    bool SendResponseMessage(
        CmdMsg &msg,
        uint16_t res_sn,
        uint32_t req_crc);

public:
    virtual void OnReceive(const void *pkt, const SocketAddr &srcaddr) override;

    virtual void RunStep(unsigned steptime) override;
};

}   // namespace net

#endif
