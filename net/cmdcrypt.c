#include <stdio.h>
#include <tomcrypt.h>
#include "cmdcrypt.h"

#pragma pack(push, 1)
struct join_sign_src
{
    uint32_t errcode;
    uint32_t userid;
};
#pragma pack(pop)

bool cmdcrypt_global_init(void)
{
    ltc_mp = ltm_desc;

    if( register_prng(&sprng_desc) )
    {
        fprintf(stderr, "ERROR: Register PRNG module failed!");
        return false;
    }

    if( register_hash(&sha256_desc) )
    {
        fprintf(stderr, "ERROR: Register SHA256 module failed!");
        return false;
    }

    if( register_cipher(&aes_desc) )
    {
        fprintf(stderr, "ERROR: Register AES module failed!");
        return false;
    }

    return true;
}

void cmdcrypt_get_skey(void *key)
{
    uint8_t *buf = key;
    size_t size = CMDCRYPT_SKEY_SIZE;

    while( size )
    {
        size_t wsz = rng_get_bytes(buf, size, NULL);
        assert( wsz <= size );

        buf += wsz;
        size -= wsz;
    }
}

bool cmdcrypt_gen_ecckey(
    void /*in*/ *privkey_buf,
    size_t /*in,out*/ *privkey_size,
    void /*in*/ *pubkey_buf,
    size_t /*in,out*/ *pubkey_size)
{
    ecc_key key;
    bool keyobj_created = false;

    bool succ = false;
    do
    {
        keyobj_created = !ecc_make_key(
            NULL,
            find_prng(CMDCRYPT_RNG_MODULE),
            CMDCRYPT_ECC_KEY_SIZE,
            &key);
        if( !keyobj_created )
            break;

        unsigned long tmpsize = *privkey_size;
        if( ecc_export(privkey_buf, &tmpsize, PK_PRIVATE, &key) )
            break;
        *privkey_size = tmpsize;

        tmpsize = *pubkey_size;
        if( ecc_export(pubkey_buf, &tmpsize, PK_PUBLIC, &key) )
            break;
        *pubkey_size = tmpsize;

        succ = true;
    } while(false);

    if( keyobj_created )
        ecc_free(&key);

    return succ;
}

size_t cmdcrypt_ecc_encrypt(
    void *dstbuf,
    size_t dstsize,
    const void *srcbuf,
    size_t srcsize,
    const void *pubkey,
    size_t keysize)
{
    ecc_key key;
    bool keyobj_created = false;

    size_t fillsize = 0;
    do
    {
        keyobj_created = !ecc_import(pubkey, keysize, &key);
        if( !keyobj_created ) break;

        unsigned long tmpsize = dstsize;
        if( ecc_encrypt_key(
            srcbuf,
            srcsize,
            dstbuf,
            &tmpsize,
            NULL,
            find_prng(CMDCRYPT_RNG_MODULE),
            find_hash(CMDCRYPT_HASH_MODULE),
            &key) )
        {
            break;
        }

        fillsize = tmpsize;
    } while(false);

    if( keyobj_created )
        ecc_free(&key);

    return fillsize;
}

size_t cmdcrypt_ecc_decrypt(
    void *dstbuf,
    size_t dstsize,
    const void *srcbuf,
    size_t srcsize,
    const void *privkey,
    size_t keysize)
{
    ecc_key key;
    bool keyobj_created = false;

    size_t fillsize = 0;
    do
    {
        keyobj_created = !ecc_import(privkey, keysize, &key);
        if( !keyobj_created ) break;

        unsigned long tmpsize = dstsize;
        if( ecc_decrypt_key(srcbuf, srcsize, dstbuf, &tmpsize, &key) ) break;

        fillsize = tmpsize;
    } while(false);

    if( keyobj_created )
        ecc_free(&key);

    return fillsize;
}

size_t cmdcrypt_ecc_sign_join_res(
    void *dstbuf,
    size_t dstsize,
    int errcode,
    int userid,
    const void *privkey,
    size_t keysize)
{
    ecc_key key;
    bool keyobj_created = false;

    size_t fillsize = 0;
    do
    {
        keyobj_created = !ecc_import(privkey, keysize, &key);
        if( !keyobj_created ) break;

        struct join_sign_src data =
        {
            .errcode = htobe32(errcode),
            .userid = htobe32( userid > 0 ? userid : 0 ),
        };

        unsigned long tmpsize = dstsize;
        if( ecc_sign_hash(
            (const unsigned char*) &data,
            sizeof(data),
            dstbuf,
            &tmpsize,
            NULL,
            find_prng(CMDCRYPT_RNG_MODULE),
            &key) )
        {
            break;
        }

        fillsize = tmpsize;
    } while(false);

    if( keyobj_created )
        ecc_free(&key);

    return fillsize;
}

bool cmdcrypt_ecc_verify_join_res(
    const void *signdata,
    size_t signsize,
    int errcode,
    int userid,
    const void *pubkey,
    size_t keysize)
{
    ecc_key key;
    bool keyobj_created = false;

    bool succ = false;
    do
    {
        keyobj_created = !ecc_import(pubkey, keysize, &key);
        if( !keyobj_created ) break;

        struct join_sign_src data =
        {
            .errcode = htobe32(errcode),
            .userid = htobe32( userid > 0 ? userid : 0 ),
        };

        int verify_result;
        if( ecc_verify_hash(
            signdata,
            signsize,
            (const unsigned char*) &data,
            sizeof(data),
            &verify_result,
            &key) )
        {
            break;
        }

        if( !verify_result ) break;

        succ = true;
    } while(false);

    if( keyobj_created )
        ecc_free(&key);

    return succ;
}

size_t cmdcrypt_calc_mac(
    void *dstbuf,
    const void *srcbuf,
    size_t srcsize,
    const void *skey)
{
    unsigned long tmpsize = CMDCRYPT_BLOCK_SIZE;
    if( pmac_memory(
        find_cipher(CMDCRYPT_SYM_MODULE),
        skey,
        CMDCRYPT_SKEY_SIZE,
        srcbuf,
        srcsize,
        dstbuf,
        &tmpsize) )
    {
        return 0;
    }

    assert( tmpsize == CMDCRYPT_BLOCK_SIZE );
    return CMDCRYPT_BLOCK_SIZE;
}

size_t cmdcrypt_calc_passphrase(
    void *dstbuf,
    const char *password,
    const void *skey)
{
    static const char default_password_source[] =
    { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };

    if( !password || !password[0] )
        password = default_password_source;

    return cmdcrypt_calc_mac(dstbuf, password, strlen(password), skey);
}
