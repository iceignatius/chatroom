#include <stdexcept>
#include "net/cmdpkt.h"
#include "CmdExchangerImpl.h"

using namespace std;
using namespace net;

void CmdExchangerImpl::Open(unsigned port, bool enable_broadcast)
{
    Close();

    if( !sock.Open(SocketAddr(ipv4_const_any, port)) )
        throw runtime_error("Open listen socket failed!");

    if( enable_broadcast && !sock.EnableBroadcast() )
        throw runtime_error("Enable UDP broadcast failed!");
}

void CmdExchangerImpl::Close()
{
    sock.Close();
}

SocketUdp* CmdExchangerImpl::GetSocket()
{
    return &sock;
}

void CmdExchangerImpl::AddPeer(CmdPeer *peer)
{
    peerlist[peer->UserId()] = shared_ptr<CmdPeer>(peer);
}

void CmdExchangerImpl::RemovePeer(int userid)
{
    peerlist.erase(userid);
}

void CmdExchangerImpl::RemoveAllPeers()
{
    peerlist.clear();
}

unsigned CmdExchangerImpl::ReceiveDispatch()
{
    SocketAddr srcaddr;

    vector<uint8_t> pkt(4096);
    int pktsize = sock.ReceiveFrom(pkt.data(), pkt.size(), srcaddr);
    if( pktsize <= 0 ) return 0;

    pkt.resize(pktsize);
    if( cmdpkt_verify_format(pkt.data(), pkt.size()) ) return 1;
    if( cmdpkt_verify_crc(pkt.data()) ) return 1;

    int srcid = cmdpkt_get_sender(pkt.data());
    auto iter = peerlist.find(srcid);
    if( iter == peerlist.end() ) return 1;

    iter->second->OnReceive(pkt.data(), srcaddr);

    return 1;
}

void CmdExchangerImpl::RunStep(unsigned steptime)
{
    ReceiveDispatch();

    for(auto iter : peerlist)
        iter.second->RunStep(steptime);
}
