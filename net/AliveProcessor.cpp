#include "common/tmutl.h"
#include "net/netdef.h"
#include "AliveProcessor.h"

using namespace net;

AliveProcessor::AliveProcessor(CmdPeer *peer) :
    CmdProcessorBase(CMDID_ALIVE, CmdProcessorBase::ACCUMULATE_SN, peer),
    peer(peer)
{
}

void AliveProcessor::RunStep(unsigned steptime)
{
    CmdProcessorBase::RunStep(steptime);

    unsigned idle_send_time = tmutl_get_mstime() - peer->LastSendTime();
    if( idle_send_time >= NETDEF_ALIVE_SEND_PERIOD && peer->IsActivated() )
    {
        CmdMsg msg(CmdId());
        msg.flags = CMDFLAG_NO_RESP;
        SendRequestMessage(msg);
    }

    unsigned idle_recv_time = tmutl_get_mstime() - peer->LastReceiveTime();
    if( idle_recv_time >= NETDEF_ALIVE_CHECK_PERIOD )
    {
        peer->OnConnectionError();
    }
}
