cmake_minimum_required(VERSION 3.5)
project(chatroom)

find_library(HAVE_NCURSES ncurses)
message("Find ncurses: ${HAVE_NCURSES}")

find_library(HAVE_NDOBJ ndobj)
message("Find ndobj: ${HAVE_NDOBJ}")

find_library(HAVE_NETSOCK netsock)
message("Find netsock: ${HAVE_NETSOCK}")

find_library(HAVE_TOMCRYPT tomcrypt)
message("Find tomcrypt: ${HAVE_TOMCRYPT}")

find_library(HAVE_TOMMATH tommath)
message("Find tommath: ${HAVE_TOMMATH}")

add_subdirectory(chatroom)
add_subdirectory(chatman)
