# Chat Room

This is an experimental project for practice and for fun.

This project implements a set of the chat room server and client.

## Software Dependency

* The [netsock](https://gitlab.com/iceignatius/netsock) library.
* The [ndobj](https://gitlab.com/iceignatius/ndobj) library.
* The [NCurses](https://invisible-island.net/ncurses/announce.html) library.
* The [LibTomCrypt](https://www.libtom.net/LibTomCrypt/) library.
