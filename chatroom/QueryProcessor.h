#ifndef _QUERY_PROCESSOR_H_
#define _QUERY_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

struct QueryInfo
{
    unsigned curr_joined_num;
    unsigned max_joined_num;
};

class QueryProcessor : public net::CmdProcessorBase
{
private:
    std::string name;
    std::string password;
    std::vector<uint8_t> pubkey;

    void *eventhost;
    QueryInfo(*OnQuery)(void *host);

public:
    QueryProcessor(
        net::CmdPeer *peer,
        const std::string &name,
        const std::string &password,
        const std::vector<uint8_t> &pubkey,
        void *eventhost,
        QueryInfo(*OnQuery)(void*));

private:
    void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;
};

#endif
