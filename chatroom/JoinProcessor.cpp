#include "net/cmdcrypt.h"
#include "JoinProcessor.h"

using namespace std;
using namespace net;

JoinProcessor::JoinProcessor(
    CmdPeer *peer,
    const string &password,
    const vector<uint8_t> &privkey,
    void *eventhost,
    int(*OnJoin)(void*, const SocketAddr&, const vector<uint8_t>&, const string&, int&)) :
        CmdProcessorBase(CMDID_JOIN, 0, peer),
        password(password),
        privkey(privkey)
{
    this->eventhost = eventhost;
    this->OnJoin = OnJoin;
}

int JoinProcessor::GetAndVerifySessionKey(
    const CmdMsg &msg,
    vector<uint8_t> &skey) const
{
    skey.clear();
    if( !privkey.size() ) return 0;

    vector<uint8_t> encrypted_skey = msg.GetBinaryArg("session-key");
    if( !encrypted_skey.size() ) return CMDRC_ERR_AUTH_REQUIRED;

    skey = CmdCrypt::EccDecrypt(encrypted_skey, privkey);
    if( skey.size() != CMDCRYPT_SKEY_SIZE ) return CMDRC_ERR_PASSPHRASE;

    vector<uint8_t> user_passphrase = msg.GetBinaryArg("passphrase");
    if( !user_passphrase.size() ) return CMDRC_ERR_PASSPHRASE;

    vector<uint8_t> calc_passphrase = CmdCrypt::CalculatePassphrase(password, skey);
    if( !calc_passphrase.size() ) return CMDRC_ERR_PASSPHRASE;

    if( user_passphrase != calc_passphrase ) return CMDRC_ERR_PASSPHRASE;

    return 0;
}

void JoinProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    vector<uint8_t> skey;
    int err = GetAndVerifySessionKey(msg, skey);

    int userid = -1;
    if( !err )
    {
        string username = msg.GetStringArg("name");
        err = OnJoin(eventhost, srcaddr, skey, username, userid);
    }

    vector<uint8_t> signature;
    if( privkey.size() )
        signature = CmdCrypt::EccSignJoinRes(err, userid, privkey);

    CmdMsg respmsg(CmdId());
    respmsg.sn = msg.sn + 1;

    respmsg.AddErrorCode(err);
    if( !err )
        respmsg.AddIntegerArg("client-id", userid);
    if( signature.size() )
        respmsg.AddBinaryArg("signature", signature);

    SendUnmanagedMessage(respmsg, srcaddr);
}
