#include "TalkProcessor.h"

using namespace std;
using namespace net;

TalkProcessor::TalkProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnTalk)(void*, const string&)) :
        CmdProcessorBase(
            CMDID_TALK,
            CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
            peer)
{
    this->eventhost = eventhost;
    this->OnTalk = OnTalk;
}

void TalkProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    OnTalk(eventhost, msg.GetStringArg("text"));

    CmdMsg respmsg(CmdId());
    respmsg.AddErrorCode();

    SendResponseMessage(respmsg, msg.sn, crc);
}

bool TalkProcessor::SendCommand(const string &sender, const string &text)
{
    CmdMsg msg(CmdId());

    msg.AddStringArg("sender", sender);
    msg.AddStringArg("text", text);

    return SendRequestMessage(msg);
}
