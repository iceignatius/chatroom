#include "net/cmdcrypt.h"
#include "JoinProcessor.h"
#include "AnonPeer.h"

using namespace std;

AnonPeer::AnonPeer(
    SocketUdp *sock,
    const string &name,
    const string &password,
    void *eventhost,
    QueryInfo(*OnQuery)(void*),
    int(*OnJoin)(void*, const SocketAddr&, const vector<uint8_t>&, const string&, int&)) :
        CmdPeerBase(CMDSENDER_ANON, sock)
{
    vector<uint8_t> privkey, pubkey;
    CmdCrypt::GenerateEccKey(privkey, pubkey);

    AddProcessor(new QueryProcessor(this, name, password, pubkey, eventhost, OnQuery));
    AddProcessor(new JoinProcessor(this, password, privkey, eventhost, OnJoin));
}
