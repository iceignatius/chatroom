#ifndef _CHATROOM_H_
#define _CHATROOM_H_

#include "net/CmdExchangerImpl.h"
#include "AnonPeer.h"
#include "ChatMan.h"

class ChatRoom
{
private:
    net::CmdExchangerImpl exgr;

    static const int max_user_num = 8;
    std::map<int, std::shared_ptr<ChatMan>> userlist;
    std::map<int, std::shared_ptr<ChatMan>> removinglist;

public:
    ChatRoom(const std::string &name, const std::string &password);
    ~ChatRoom();

public:
    void Open(unsigned port);
    void Close();

    void RunStep(unsigned steptime);

private:
    int GenerateUserId() const;

    void RemoveUserTotally(int userid);
    void RemoveUserLater(int userid);

private:
    static QueryInfo EventOnQuery(void *self);
    static int EventOnJoin(
        void *self,
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        const std::string &username,
        int &userid);
    static void EventOnQuit(void *self, int userid);
    static void EventOnError(void *self, int userid);
    static void EventOnTalk(void *self, int userid, const std::string &text);

    QueryInfo OnQuery();
    int OnJoin(
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        const std::string &username,
        int &userid);
    void OnQuit(int userid);
    void OnError(int userid);
    void OnTalk(int userid, const std::string &text);

    void BroadcastTalk(const std::string &sender, const std::string &text);
};

#endif
