#ifndef _QUIT_PROCESSOR_H_
#define _QUIT_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class QuitProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnQuit)(void *host);

public:
    QuitProcessor(net::CmdPeer *peer, void *eventhost, void(*OnQuit)(void*));

private:
    virtual void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;
};

#endif
