#include "ChatRoom.h"

using namespace std;

#define LOGLABEL "ChatRoom"

ChatRoom::ChatRoom(const string &name, const string &password)
{
    exgr.AddPeer(new AnonPeer(
        exgr.GetSocket(),
        name,
        password,
        this,
        EventOnQuery,
        EventOnJoin));
}

ChatRoom::~ChatRoom()
{
    Close();
}

void ChatRoom::Open(unsigned port)
{
    exgr.Open(port);
}

void ChatRoom::Close()
{
    exgr.Close();

    userlist.clear();
    removinglist.clear();
    exgr.RemoveAllPeers();
}

void ChatRoom::RunStep(unsigned steptime)
{
    while( exgr.ReceiveDispatch() )
    {}

    exgr.RunStep(steptime);

    for(auto iter : userlist)
        iter.second->RunStep(steptime);

    for(auto iter : removinglist)
        iter.second->RunStep(steptime);

    for(auto iter = removinglist.cbegin(); iter != removinglist.cend(); )
    {
        if( iter->second->CanBeRemoved() )
            RemoveUserTotally((iter++)->first);
        else
            ++iter;
    }
}

int ChatRoom::GenerateUserId() const
{
    if( userlist.size() >= max_user_num )
        return -1;

    if( userlist.size() == 0 )
        return 1;

    auto iter = userlist.crbegin();
    if( iter == userlist.crend() )
        return 1;

    return iter->first + 1;
}

void ChatRoom::RemoveUserTotally(int userid)
{
    userlist.erase(userid);
    removinglist.erase(userid);
}

void ChatRoom::RemoveUserLater(int userid)
{
    auto iter = userlist.find(userid);
    if( iter == userlist.end() ) return;

    auto user = iter->second;
    user->StartDelayedRemove();

    removinglist[iter->first] = user;
    userlist.erase(iter);
}

QueryInfo ChatRoom::EventOnQuery(void *self)
{
    return ((ChatRoom*)self)->OnQuery();
}

int ChatRoom::EventOnJoin(
    void *self,
    const SocketAddr &remote,
    const vector<uint8_t> &skey,
    const string &username,
    int &userid)
{
    return ((ChatRoom*)self)->OnJoin(remote, skey, username, userid);
}

void ChatRoom::EventOnQuit(void *self, int userid)
{
    ((ChatRoom*)self)->OnQuit(userid);
}

void ChatRoom::EventOnError(void *self, int userid)
{
    ((ChatRoom*)self)->OnError(userid);
}

void ChatRoom::EventOnTalk(void *self, int userid, const string &text)
{
    ((ChatRoom*)self)->OnTalk(userid, text);
}

QueryInfo ChatRoom::OnQuery()
{
    printf("%s: On query.\n", LOGLABEL);

    QueryInfo info;
    info.curr_joined_num = userlist.size();
    info.max_joined_num = max_user_num;

    return info;
}

int ChatRoom::OnJoin(
    const SocketAddr &remote,
    const vector<uint8_t> &skey,
    const string &username,
    int &userid)
{
    printf("%s: On join: name=\"%s\"\n", LOGLABEL, username.c_str());

    userid = GenerateUserId();
    if( userid <= 0 )
    {
        printf(
            "ChatRoom: Failed to add new user: name=\"%s\", reason=\"%s\"\n",
            username.c_str(),
            "User pool full");
        return CMDRC_ERR_JOIN_FULL;
    }

    auto iter = removinglist.find(userid);
    if( iter != removinglist.end() )
        removinglist.erase(iter);

    auto user = shared_ptr<ChatMan>(new ChatMan(
        &exgr,
        remote,
        skey,
        userid,
        username,
        this,
        EventOnError,
        EventOnQuit,
        EventOnTalk));
    userlist[userid] = user;

    printf(
        "ChatRoom: An user joined: name=\"%s\", id=%d\n",
        username.c_str(),
        userid);

    return 0;
}

void ChatRoom::OnQuit(int userid)
{
    auto iter = userlist.find(userid);
    if( iter == userlist.end() ) return;

    auto user = iter->second;
    RemoveUserLater(user->GetId());

    printf(
        "%s: On quit: name=\"%s\", id=%d\n",
        LOGLABEL,
        user->GetName().c_str(),
        userid);
}

void ChatRoom::OnError(int userid)
{
    auto iter = userlist.find(userid);
    if( iter == userlist.end() ) return;

    auto user = iter->second;
    RemoveUserLater(user->GetId());

    printf(
        "%s: An user dropped: name=\"%s\", id=%d\n",
        LOGLABEL,
        user->GetName().c_str(),
        userid);
}

void ChatRoom::OnTalk(int userid, const string &text)
{
    auto iter = userlist.find(userid);
    if( iter == userlist.end() ) return;

    auto sender = iter->second;
    BroadcastTalk(sender->GetName(), text);
}

void ChatRoom::BroadcastTalk(const string &sender, const string &text)
{
    for(auto iter : userlist)
    {
        auto user = iter.second;
        user->SendTalk(sender, text);
    }
}
