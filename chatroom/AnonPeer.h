#ifndef _ANON_PEER_H_
#define _ANON_PEER_H_

#include "net/CmdPeerBase.h"
#include "QueryProcessor.h"

class AnonPeer : public net::CmdPeerBase
{
public:
    AnonPeer(
        SocketUdp *sock,
        const std::string &name,
        const std::string &password,
        void *eventhost,
        QueryInfo(*OnQuery)(void*),
        int(*OnJoin)(void*, const SocketAddr&, const std::vector<uint8_t>&, const std::string&, int&));
};

#endif
