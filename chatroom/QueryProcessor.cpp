#include "QueryProcessor.h"

using namespace std;
using namespace net;

QueryProcessor::QueryProcessor(
    CmdPeer *peer,
    const string &name,
    const string &password,
    const vector<uint8_t> &pubkey,
    void *eventhost,
    QueryInfo(*OnQuery)(void*)) :
        CmdProcessorBase(CMDID_QUERY, 0, peer),
        name(name),
        password(password),
        pubkey(pubkey)
{
    this->eventhost = eventhost;
    this->OnQuery = OnQuery;
}

void QueryProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    QueryInfo info = OnQuery(eventhost);

    CmdMsg respmsg(CmdId());
    respmsg.sn = msg.sn + 1;

    if( name.length() )
        respmsg.AddStringArg("name", name);
    respmsg.AddUnsignedArg("curr-joined-num", info.curr_joined_num);
    respmsg.AddUnsignedArg("max-joined-num", info.max_joined_num);
    if( password.length() )
        respmsg.AddBooleanArg("need-password", true);
    if( pubkey.size() )
        respmsg.AddBinaryArg("public-key", pubkey);

    SendUnmanagedMessage(respmsg, srcaddr);
}
