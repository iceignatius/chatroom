#include "QuitProcessor.h"

using namespace std;
using namespace net;

QuitProcessor::QuitProcessor(CmdPeer *peer, void *eventhost, void(*OnQuit)(void*)) :
    CmdProcessorBase(
        CMDID_QUIT,
        CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
        peer)
{
    this->eventhost = eventhost;
    this->OnQuit = OnQuit;
}

void QuitProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    OnQuit(eventhost);

    CmdMsg respmsg(CmdId());
    respmsg.AddErrorCode();

    SendResponseMessage(respmsg, msg.sn, crc);
}
