#ifndef _TALK_PROCESSOR_H_
#define _TALK_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class TalkProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnTalk)(void *host, const std::string &text);

public:
    TalkProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnTalk)(void*, const std::string&));

private:
    virtual void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;

public:
    bool SendCommand(const std::string &sender, const std::string &text);
};

#endif
