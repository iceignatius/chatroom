#ifndef _CHAT_PEER_H_
#define _CHAT_PEER_H_

#include "net/CmdPeerBase.h"
#include "TalkProcessor.h"

class ChatPeer : public net::CmdPeerBase
{
private:
    void *eventhost;
    void(*OnError)(void*);

    TalkProcessor *talk_proc;

public:
    ChatPeer(
        int userid,
        SocketUdp *sock,
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        void *eventhost,
        void(*OnError)(void*),
        void(*OnQuit)(void*),
        void(*OnTalk)(void*, const std::string&));

public:
    virtual void OnConnectionError() override;

    bool SendTalk(const std::string &sender, const std::string &text);
};

#endif
