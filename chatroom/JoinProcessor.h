#ifndef _JOIN_PROCESSOR_H_
#define _JOIN_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class JoinProcessor : public net::CmdProcessorBase
{
private:
    std::string password;
    std::vector<uint8_t> privkey;

    void *eventhost;
    int(*OnJoin)(
        void *host,
        const SocketAddr &remote,
        const std::vector<uint8_t> &skey,
        const std::string &username,
        int &userid);

public:
    JoinProcessor(
        net::CmdPeer *peer,
        const std::string &password,
        const std::vector<uint8_t> &privkey,
        void *eventhost,
        int(*OnJoin)(void*, const SocketAddr&, const std::vector<uint8_t>&, const std::string&, int&));

private:
    int GetAndVerifySessionKey(
        const net::CmdMsg &msg,
        std::vector<uint8_t> &skey) const;

    virtual void OnRequestMessage(
        const net::CmdMsg &msg,
        uint32_t crc,
        const SocketAddr &srcaddr) override;
};

#endif
