#include <signal.h>
#include <iostream>
#include <getopt.h>
#include "common/tmutl.h"
#include "net/netdef.h"
#include "net/cmdcrypt.h"
#include "ChatRoom.h"

#define TARGET_STEPTIME 100

using namespace std;

struct ServerOpts
{
    bool needhelp;
    uint16_t port;
    string name;
    string password;
    bool cipher_enabled;
};

static bool terminating = false;

static
void ProcessSignalHandler(int sig)
{
    terminating = true;
}

static
void PrintHelp(const string &apname)
{
    cout << "chatroom-server" << endl;
    cout << "Usage: " << apname << " [options]" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << "  -h, --help                Show this message." << endl;
    cout << "  -p, port=port             Port of the server" << endl;
    cout << "  -m, --name=name           Name of the chat room." << endl;
    cout << "  -w, --password=password   The password to allow clients to join." << endl;
    cout << "      --disable-cipher      Disable the cipher behaviour." << endl;
}

static
ServerOpts ParseOpts(int argc, char *argv[])
{
    ServerOpts info;
    info.needhelp = false;
    info.port = NETDEF_DEFAULT_SERVER_PORT;
    info.cipher_enabled = true;

    static const struct option longopts[] =
    {
        { "help",           no_argument,        nullptr, 'h' },
        { "port",           required_argument,  nullptr, 'p' },
        { "name",           required_argument,  nullptr, 'm' },
        { "password",       required_argument,  nullptr, 'w' },
        { "disable-cipher", required_argument,  nullptr, 'D' },
        { nullptr, 0, nullptr, 0 }
    };

    static const char shortopts[] = "hp:m:w:";

    int opt, index;
    while( ( opt = getopt_long(argc, argv, shortopts, longopts, &index) ) >= 0 )
    {
        switch( opt )
        {
        case 'h':
            info.needhelp = true;
            break;

        case 'p':
            if( 0 == ( info.port = atoi(optarg) ) )
            {
                cerr << "ERROR: Invalid port format!" << endl;
                info.needhelp = true;
            }
            break;

        case 'm':
            info.name = optarg;
            break;

        case 'w':
            info.password = optarg;
            break;

        case 'D':
            info.cipher_enabled = false;
            break;

        case '?':
            cerr << "ERROR: Unsupported option: \"" << optopt << "\"" << endl;
            info.needhelp = true;
            break;
        }
    }

    return info;
}

int main(int argc, char *argv[])
{
    ServerOpts opts = ParseOpts(argc, argv);
    if( opts.needhelp )
    {
        PrintHelp(argv[0]);
        return 0;
    }

    int exitcode = 1;
    try
    {
        signal(SIGINT, ProcessSignalHandler);
#ifndef _WIN32
        signal(SIGQUIT, ProcessSignalHandler);
#endif

        if( !CmdCrypt::GlobalInit() )
            throw runtime_error("Initialise cipher module failed!");

        ChatRoom chatroom(opts.name, opts.password);
        chatroom.Open(opts.port);

        unsigned lasttime = tmutl_get_mstime();
        while( !terminating )
        {
            unsigned currtime = tmutl_get_mstime();
            unsigned steptime = currtime - lasttime;
            lasttime = currtime;

            chatroom.RunStep(steptime);

            unsigned proctime = tmutl_get_mstime() - currtime;
            if( proctime < TARGET_STEPTIME )
                tmutl_sleep( TARGET_STEPTIME - proctime );
        }

        exitcode = 0;
    }
    catch(exception &e)
    {
        cerr << "ERROR: " << e.what() << endl;
    }
    catch(...)
    {
        cerr << "ERROR: Unknown error occurred!" << endl;
    }

    return exitcode;
}
