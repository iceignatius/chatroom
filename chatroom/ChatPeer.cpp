#include "net/AliveProcessor.h"
#include "QuitProcessor.h"
#include "ChatPeer.h"

using namespace std;
using namespace net;

ChatPeer::ChatPeer(
    int userid,
    SocketUdp *sock,
    const SocketAddr &remote,
    const vector<uint8_t> &skey,
    void *eventhost,
    void(*OnError)(void*),
    void(*OnQuit)(void*),
    void(*OnTalk)(void*, const string&)) :
        CmdPeerBase(userid, sock, skey)
{
    this->eventhost = eventhost;
    this->OnError = OnError;

    Active(remote);

    AliveProcessor *alive_proc = new AliveProcessor(this);
    QuitProcessor *quit_proc = new QuitProcessor(this, eventhost, OnQuit);
    talk_proc = new TalkProcessor(this, eventhost, OnTalk);

    AddProcessor(alive_proc);
    AddProcessor(quit_proc);
    AddProcessor(talk_proc);
}

void ChatPeer::OnConnectionError()
{
    OnError(eventhost);
    CmdPeerBase::OnConnectionError();
}

bool ChatPeer::SendTalk(const string &sender, const string &text)
{
    return talk_proc->SendCommand(sender, text);
}
