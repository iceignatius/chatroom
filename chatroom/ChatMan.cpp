#include "net/netdef.h"
#include "ChatMan.h"

using namespace std;

ChatMan::ChatMan(
    net::CmdExchanger *exgr,
    const SocketAddr &addr,
    const vector<uint8_t> &skey,
    int id,
    const string &name,
    void *eventhost,
    void(*OnUserError)(void*, int),
    void(*OnUserQuit)(void*, int),
    void(*OnUserTalk)(void*, int, const string&)) :
        exgr(exgr),
        id(id),
        name(name),
        waiting_remove(false),
        removing_timer(0)
{
    peer = new ChatPeer(
        id,
        exgr->GetSocket(),
        addr,
        skey,
        this,
        EventOnError,
        EventOnQuit,
        EventOnTalk);
    exgr->AddPeer(peer);

    this->eventhost = eventhost;
    this->OnUserError = OnUserError;
    this->OnUserQuit = OnUserQuit;
    this->OnUserTalk = OnUserTalk;
}

ChatMan::~ChatMan()
{
    exgr->RemovePeer(id);
}

bool ChatMan::CanBeRemoved() const
{
    return waiting_remove && !removing_timer;
}

void ChatMan::StartDelayedRemove()
{
    waiting_remove = true;
    removing_timer = NETDEF_QUIT_REMOVE_TIMEOUT;
}

void ChatMan::RunStep(unsigned steptime)
{
    if( waiting_remove )
        removing_timer -= removing_timer > steptime ? steptime : removing_timer;
}

void ChatMan::SendTalk(const string &sender, const string &text)
{
    peer->SendTalk(sender, text);
}

void ChatMan::EventOnError(void *self)
{
    ((ChatMan*)self)->OnError();
}

void ChatMan::EventOnQuit(void *self)
{
    ((ChatMan*)self)->OnQuit();
}

void ChatMan::EventOnTalk(void *self, const string &text)
{
    ((ChatMan*)self)->OnTalk(text);
}

void ChatMan::OnError()
{
    OnUserError(eventhost, id);
}

void ChatMan::OnQuit()
{
    OnUserQuit(eventhost, id);
}

void ChatMan::OnTalk(const string &text)
{
    if( !IsActivated() ) return;

    OnUserTalk(eventhost, id, text);
}
