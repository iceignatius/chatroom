#ifndef _CHAT_MAN_H_
#define _CHAT_MAN_H_

#include "net/CmdExchanger.h"
#include "ChatPeer.h"

class ChatMan
{
private:
    net::CmdExchanger *exgr;
    ChatPeer *peer;

    int id;
    std::string name;

    bool waiting_remove;
    unsigned removing_timer;

    void *eventhost;
    void(*OnUserError)(void *host, int userid);
    void(*OnUserQuit)(void *host, int userid);
    void(*OnUserTalk)(void *host, int userid, const std::string &text);

public:
    ChatMan(
        net::CmdExchanger *exgr,
        const SocketAddr &addr,
        const std::vector<uint8_t> &skey,
        int id,
        const std::string &name,
        void *eventhost,
        void(*OnUserError)(void*, int),
        void(*OnUserQuit)(void*, int),
        void(*OnUserTalk)(void*, int, const std::string&));
    ~ChatMan();

public:
    int GetId() const { return id; }
    const std::string& GetName() const { return name; }

    bool IsActivated() const { return !waiting_remove; }
    bool CanBeRemoved() const;
    void StartDelayedRemove();

public:
    void RunStep(unsigned steptime);

    void SendTalk(const std::string &sender, const std::string &text);

private:
    static void EventOnError(void *self);
    static void EventOnQuit(void *self);
    static void EventOnTalk(void *self, const std::string &text);

    void OnError();
    void OnQuit();
    void OnTalk(const std::string &text);
};

#endif
