#ifndef _CONNECT_PEER_H_
#define _CONNECT_PEER_H_

#include "net/CmdPeerBase.h"
#include "QueryProcessor.h"
#include "JoinProcessor.h"

class ConnectPeer : public net::CmdPeerBase
{
private:
    QueryProcessor *query_proc;
    JoinProcessor *join_proc;

public:
    ConnectPeer(
        SocketUdp *sock,
        void *eventhost,
        void(*OnQueryResp)(void*, const ServerInfo&),
        void(*OnJoinResp)(void*, int, int));

public:
    bool SendQueryCommand();
    bool SendJoinCommand(
        const std::string &username,
        const std::string &password,
        const std::vector<uint8_t> &pubkey,
        const std::vector<uint8_t> &skey);
};

#endif
