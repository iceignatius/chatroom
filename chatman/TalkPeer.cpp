#include "net/AliveProcessor.h"
#include "TalkPeer.h"

using namespace std;
using namespace net;

TalkPeer::TalkPeer(
    int userid,
    SocketUdp *sock,
    const vector<uint8_t> &skey,
    void *eventhost,
    void(*OnError)(void*),
    void(*OnQuitResp)(void*),
    void(*OnTalkReq)(void*, const string&, const string&)) :
        CmdPeerBase(userid, sock, skey)
{
    this->eventhost = eventhost;
    this->OnError = OnError;

    AliveProcessor *alive_proc = new AliveProcessor(this);
    quit_proc = new QuitProcessor(this, eventhost, OnQuitResp);
    talk_proc = new TalkProcessor(this, eventhost, OnTalkReq);

    AddProcessor(alive_proc);
    AddProcessor(quit_proc);
    AddProcessor(talk_proc);
}

void TalkPeer::OnConnectionError()
{
    OnError(eventhost);
    CmdPeerBase::OnConnectionError();
}

bool TalkPeer::SendQuitCommand()
{
    return quit_proc->SendCommand();
}

bool TalkPeer::SendTalkCommand(const string &text)
{
    return talk_proc->SendCommand(text);
}
