#ifndef _TEXT_INPUT_H_
#define _TEXT_INPUT_H_

#include <string>
#include <list>
#include <ncurses.h>

class TextInput
{
private:
    std::list<std::string> completed_queue;
    std::string buffered_text;

public:
    void PushCharacter(int ch);
    void PushKeyState(const bool key_state[KEY_MAX]);

    void ClearBuffered();
    void ClearAll();

    bool HaveBufferedText() const;
    std::string GetBufferedText() const;

    bool HaveCompletedText() const;
    std::string PopCompletedText();
};

#endif
