#ifndef _CHATMAN_H_
#define _CHATMAN_H_

#include "net/CmdExchangerImpl.h"
#include "ConnectPeer.h"
#include "TalkPeer.h"

class ChatMan
{
public:
    enum class State
    {
        Disconnected,
        Error,
        Query,
        Join,
        Talk,
    };

private:
    struct TalkInfo
    {
        std::string sender;
        std::string text;
    };

private:
    State state;
    std::string state_msg;

    net::CmdExchangerImpl exgr;
    ConnectPeer *connect_peer;
    TalkPeer *talk_peer;

    struct ConnectInfo
    {
        std::string username;
        std::string password;
        SocketAddr remote_addr;
        int timeout;
    } connect_info;

    int userid;
    std::vector<uint8_t> skey;

    std::list<TalkInfo> remote_talk_queue;

public:
    ChatMan();

public:
    State GetState(std::string *msg = nullptr) const;

    void Connect(
        const SocketAddr &addr,
        const std::string &username,
        const std::string &password);
    void Disconnect(const std::string &msg = "");

    void Update(unsigned steptime);

private:
    static void EventOnQueryResp(void *self, const ServerInfo &info);
    static void EventOnJoinResp(void *self, int errcode, int userid);
    static void EventOnQuitResp(void *self);
    static void EventOnError(void *self);
    static void EventOnTalkReq(
        void *self,
        const std::string &sender,
        const std::string &text);

    void OnQueryResp(const ServerInfo &info);
    void OnJoinResp(int errcode, int userid);
    void OnQuitResp();
    void OnError();
    void OnTalkReq(const std::string &sender, const std::string &text);

public:
    void SendText(const std::string &text);
    bool ReceiveText(std::string &text, std::string &sender);
};

#endif
