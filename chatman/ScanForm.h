#ifndef _SCAN_FORM_H_
#define _SCAN_FORM_H_

#include <vector>
#include "ui/FormBase.h"
#include "net/CmdExchangerImpl.h"
#include "ScanPeer.h"

class ScanForm : public ui::FormBase
{
private:
    std::string username;

    std::vector<ServerInfo> infolist;
    unsigned curr_sel;

    net::CmdExchangerImpl exgr;
    ScanPeer *scan_peer;

private:
    ScanForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    void OnPush(const ui::FormParams &params) override;
    void OnFocus(const ui::FormParams &params) override;

private:
    std::string GetPasswordInput();
    bool PushTalkForm(const ServerInfo &remote);

public:
    void Update(unsigned steptime) override;
    void Render() override;

private:
    static void EventOnServerInfo(void *self, const ServerInfo &info);

    void OnServerInfo(const ServerInfo &info);
};

#endif
