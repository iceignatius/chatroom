#include "TalkProcessor.h"

using namespace std;
using namespace net;

TalkProcessor::TalkProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnTalk)(void*, const string&, const string&)) :
        CmdProcessorBase(
            CMDID_TALK,
            CmdProcessorBase::AUTO_RESP | CmdProcessorBase::ACCUMULATE_SN,
            peer)
{
    this->eventhost = eventhost;
    this->OnTalk = OnTalk;
}

void TalkProcessor::OnRequestMessage(
    const CmdMsg &msg,
    uint32_t crc,
    const SocketAddr &srcaddr)
{
    string sender = msg.GetStringArg("sender");
    if( !sender.length() )
        sender = "unknown";

    string text = msg.GetStringArg("text");

    OnTalk(eventhost, sender, text);

    CmdMsg respmsg(CmdId());
    SendResponseMessage(respmsg, msg.sn, crc);
}

bool TalkProcessor::SendCommand(const string &text)
{
    CmdMsg msg(CmdId());

    msg.AddStringArg("text", text);

    return SendRequestMessage(msg);
}
