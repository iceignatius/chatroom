#ifndef _TALK_PEER_H_
#define _TALK_PEER_H_

#include "net/CmdPeerBase.h"
#include "QuitProcessor.h"
#include "TalkProcessor.h"

class TalkPeer : public net::CmdPeerBase
{
private:
    void *eventhost;
    void(*OnError)(void *host);

    QuitProcessor *quit_proc;
    TalkProcessor *talk_proc;

public:
    TalkPeer(
        int userid,
        SocketUdp *sock,
        const std::vector<uint8_t> &skey,
        void *eventhost,
        void(*OnError)(void*),
        void(*OnQuitResp)(void*),
        void(*OnTalkReq)(void*, const std::string&, const std::string&));

private:
    void OnConnectionError() override;

public:
    bool SendQuitCommand();
    bool SendTalkCommand(const std::string &text);
};

#endif
