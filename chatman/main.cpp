#include <iostream>
#include <getopt.h>

#ifdef _WIN32
#   include <windows.h>
#else
#   include <unistd.h>
#   include <pwd.h>
#endif

#include <netsock/urlpar.h>
#include <netsock/urladdr.h>
#include "net/netdef.h"
#include "net/cmdcrypt.h"
#include "ui/FormManagerImpl.h"
#include "ui/MessageForm.h"
#include "ui/TextInputForm.h"
#include "ScanForm.h"
#include "TalkForm.h"

using namespace std;

struct ClientOpts
{
    bool needhelp;
    string url;
    string password;
    string name;
};

static
void PrintHelp(const std::string &apname)
{
    cout << "chatroom-client" << endl;
    cout << "Usage: " << apname << " [options]" << endl;
    cout << endl;
    cout << "Options:" << endl;
    cout << "  -h, --help                Show this message." << endl;
    cout << "  -c, --connect=url         The server address." << endl;
    cout << "  -w, --password=password   The password to join the server." << endl;
    cout << "  -m, --name=name           The user name to show in the chat." << endl;
}

static
string GetLoginName()
{
#ifdef _WIN32
    char name[64] = {0};
    DWORD size = sizeof(name);
    return GetUserNameA(name, &size) ? name : "";
#else
    struct passwd *pw = getpwuid(geteuid());
    return pw ? pw->pw_name : "";
#endif
}

static
ClientOpts ParseOpts(int argc, char *argv[])
{
    ClientOpts info;
    info.needhelp = false;
    info.name = GetLoginName();

    static const struct option longopts[] =
    {
        { "help",       no_argument,        nullptr, 'h' },
        { "connect",    required_argument,  nullptr, 'c' },
        { "password",   required_argument,  nullptr, 'w' },
        { "name",       required_argument,  nullptr, 'm' },
        { nullptr, 0, nullptr, 0 }
    };

    static const char shortopts[] = "hc:w:m:";

    int opt, index;
    while( ( opt = getopt_long(argc, argv, shortopts, longopts, &index) ) >= 0 )
    {
        switch( opt )
        {
        case 'h':
            info.needhelp = true;
            break;

        case 'c':
            info.url = optarg;
            break;

        case 'w':
            info.password = optarg;
            break;

        case 'm':
            info.name = optarg;
            break;

        case '?':
            cerr << "ERROR: Unsupported option: \"" << optopt << "\"" << endl;
            info.needhelp = true;
            break;
        }
    }

    return info;
}

int main(int argc, char *argv[])
{
    ClientOpts opts = ParseOpts(argc, argv);
    if( opts.needhelp )
    {
        PrintHelp(argv[0]);
        return 0;
    }

    try
    {
        if( !CmdCrypt::GlobalInit() )
            throw runtime_error("Initialise cipher module failed!");

        ui::FormManagerImpl form_manager;
        form_manager.Setup();

        form_manager.RegisterFormKlass(ui::MessageForm::KlassName(), ui::MessageForm::CreateInstance);
        form_manager.RegisterFormKlass(ui::TextInputForm::KlassName(), ui::TextInputForm::CreateInstance);
        form_manager.RegisterFormKlass(ScanForm::KlassName(), ScanForm::CreateInstance);
        form_manager.RegisterFormKlass(TalkForm::KlassName(), TalkForm::CreateInstance);

        if( opts.url.length() )
        {
            SocketAddr server_addr =
                UrlAddr::GetAddrFromUrl(opts.url, NETDEF_DEFAULT_SERVER_PORT);

            ui::FormParams params;
            params.InsertNetAddrParam("addr", server_addr);
            params.InsertStringParam("username", opts.name);
            params.InsertStringParam("password", opts.password);

            form_manager.PushNewForm("talkform", params);
        }
        else
        {
            ui::FormParams params;
            params.InsertStringParam("username", opts.name);

            form_manager.PushNewForm("scanform", params);
        }

        form_manager.RunModal();
    }
    catch(exception &e)
    {
        cerr << "ERROR: " << e.what() << endl;
    }
    catch(...)
    {
        cerr << "ERROR: Unknown error occurred!" << endl;
    }

    return 0;
}
