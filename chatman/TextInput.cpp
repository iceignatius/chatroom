#include "TextInput.h"

#define ASCII_BS    0x08
#define ASCII_CR    0x0D
#define ASCII_DEL   0x7F

using namespace std;

void TextInput::PushCharacter(int ch)
{
    switch(ch)
    {
    case ASCII_BS:
        buffered_text.pop_back();
        break;

    case ASCII_CR:
        if( !buffered_text.empty() )
        {
            completed_queue.push_back(buffered_text);
            buffered_text.clear();
        }
        break;

    case ASCII_DEL:
        buffered_text.clear();
        break;

    default:
        buffered_text.push_back(ch);
    }
}

void TextInput::PushKeyState(const bool key_state[KEY_MAX])
{
    // Scan for ASCII codes.
    for(int ch = 0; ch < 0x80; ++ch)
    {
        if( key_state[ch] )
            PushCharacter(ch);
    }

    // Scan for special keys.

    if( key_state[KEY_BACKSPACE] )
        PushCharacter(ASCII_BS);

    if( key_state[KEY_ENTER] )
        PushCharacter(ASCII_CR);

    if( key_state[KEY_DC] )
        PushCharacter(ASCII_DEL);
}

void TextInput::ClearBuffered()
{
    buffered_text.clear();
}

void TextInput::ClearAll()
{
    buffered_text.clear();
    completed_queue.clear();
}

bool TextInput::HaveBufferedText() const
{
    return !buffered_text.empty();
}

string TextInput::GetBufferedText() const
{
    return buffered_text;
}

bool TextInput::HaveCompletedText() const
{
    return !completed_queue.empty();
}

string TextInput::PopCompletedText()
{
    if( completed_queue.empty() )
        return "";

    string text = completed_queue.front();
    completed_queue.pop_front();

    return text;
}
