#include "net/netdef.h"
#include "net/cmdcrypt.h"
#include "ChatMan.h"

using namespace std;

ChatMan::ChatMan() :
    state(State::Disconnected),
    state_msg("Disconnected"),
    connect_peer(nullptr),
    talk_peer(nullptr),
    userid(CMDSENDER_ANON)
{
}

ChatMan::State ChatMan::GetState(string *msg) const
{
    if( msg )
        *msg = state_msg;

    return state;
}

void ChatMan::Connect(
    const SocketAddr &addr,
    const string &username,
    const string &password)
{
    Disconnect();

    try
    {
        CmdCrypt::GenerateSKey(skey);

        exgr.Open(0);

        connect_peer = new ConnectPeer(
            exgr.GetSocket(),
            this,
            EventOnQueryResp,
            EventOnJoinResp);
        exgr.AddPeer(connect_peer);

        connect_peer->Active(addr);

        if( !connect_peer->SendQueryCommand() )
            throw runtime_error("Send query command failed!");

        state = State::Query;
        state_msg = "Connecting...";

        connect_info.username = username;
        connect_info.password = password;
        connect_info.timeout = NETDEF_CONNECT_TIMEOUT;
    }
    catch(exception &e)
    {
        Disconnect();
        throw;
    }
}

void ChatMan::Disconnect(const string &msg)
{
    if( talk_peer )
        talk_peer->SendQuitCommand();

    userid = CMDSENDER_ANON;
    skey.clear();

    connect_info.remote_addr = SocketAddr();
    connect_info.password = "";
    connect_info.username = "";
    connect_info.timeout = 0;

    exgr.Close();
    exgr.RemoveAllPeers();
    talk_peer = nullptr;
    connect_peer = nullptr;

    state = State::Disconnected;
    state_msg = msg.length() ? msg : "Disconnected";
}

void ChatMan::Update(unsigned steptime)
{
    while( exgr.ReceiveDispatch() )
    {}

    exgr.RunStep(steptime);

    switch( state )
    {
    case State::Disconnected:
    case State::Error:
        break;

    case State::Query:
    case State::Join:
        connect_info.timeout -= steptime;
        if( connect_info.timeout <= 0 )
            Disconnect("Connection timeout!");
        break;

    case State::Talk:
        if( connect_peer )
        {
            exgr.RemovePeer(CMDSENDER_ANON);
            connect_peer = nullptr;
        }
        break;
    }
}

void ChatMan::EventOnQueryResp(void *self, const ServerInfo &info)
{
    ((ChatMan*)self)->OnQueryResp(info);
}

void ChatMan::EventOnJoinResp(void *self, int errcode, int userid)
{
    ((ChatMan*)self)->OnJoinResp(errcode, userid);
}

void ChatMan::EventOnQuitResp(void *self)
{
    ((ChatMan*)self)->OnQuitResp();
}

void ChatMan::EventOnError(void *self)
{
    ((ChatMan*)self)->OnError();
}

void ChatMan::EventOnTalkReq(
    void *self,
    const string &sender,
    const string &text)
{
    ((ChatMan*)self)->OnTalkReq(sender, text);
}

void ChatMan::OnQueryResp(const ServerInfo &info)
{
    connect_info.remote_addr = info.addr;

    if( !connect_peer->SendJoinCommand(
        connect_info.username,
        connect_info.password,
        info.pubkey,
        skey) )
    {
        throw runtime_error("Send join command failed!");
    }

    state = State::Join;
    state_msg = "Connecting...";
}

void ChatMan::OnJoinResp(int errcode, int userid)
{
    if( errcode )
    {
        Disconnect(cmdrc_to_str(errcode));
        return;
    }

    this->userid = userid;

    talk_peer = new TalkPeer(
        userid,
        exgr.GetSocket(),
        skey,
        this,
        EventOnError,
        EventOnQuitResp,
        EventOnTalkReq);
    exgr.AddPeer(talk_peer);

    talk_peer->Active(connect_info.remote_addr);

    state = State::Talk;
    state_msg = "Talking";
}

void ChatMan::OnQuitResp()
{
    // Nothing to do!
}

void ChatMan::OnError()
{
    Disconnect();
    state = State::Error;
    state_msg = "Connection error!";
}

void ChatMan::OnTalkReq(const string &sender, const string &text)
{
    TalkInfo info;
    info.sender = sender;
    info.text = text;

    remote_talk_queue.push_back(info);
}

void ChatMan::SendText(const string &text)
{
    talk_peer->SendTalkCommand(text);
}

bool ChatMan::ReceiveText(string &text, string &sender)
{
    if( !remote_talk_queue.size() ) return false;

    TalkInfo info = remote_talk_queue.front();
    remote_talk_queue.pop_front();

    sender = info.sender;
    text = info.text;

    return true;
}
