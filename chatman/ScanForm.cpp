#include "common/ascii.h"
#include "ScanForm.h"

using namespace std;
using namespace ui;

ScanForm::ScanForm(FormManager *mgr) :
    FormBase(mgr), curr_sel(0)
{
    exgr.Open(0, true);

    scan_peer = new ScanPeer(exgr.GetSocket(), this, EventOnServerInfo);
    exgr.AddPeer(scan_peer);
}

const char* ScanForm::KlassName()
{
    return "scanform";
}

Form* ScanForm::CreateInstance(FormManager *mgr)
{
    return new ScanForm(mgr);
}

void ScanForm::OnPush(const FormParams &params)
{
    username = params.GetStringParam("username", "");
}

void ScanForm::OnFocus(const FormParams &params)
{
    curr_sel = 0;
    infolist.clear();

    scan_peer->SendQuery();
}

string ScanForm::GetPasswordInput()
{
    ui::FormParams params;
    params.InsertStringParam("title", "Password");
    params.InsertStringParam("prompt", "Please input\nthe login password");

    Manager()->PushNewForm("text-input-form", params);
    if( Manager()->RunModal(this, &params) )
        return "";

    return params.GetStringParam("result", "");
}

bool ScanForm::PushTalkForm(const ServerInfo &remote)
{
    ui::FormParams params;
    params.InsertNetAddrParam("addr", remote.addr);
    params.InsertStringParam("username", username);

    if( remote.need_password )
    {
        string password = GetPasswordInput();
        if( !password.length() )
            return false;

        params.InsertStringParam("password", password);
    }

    Manager()->PushNewForm("talkform", params);
    return true;
}

void ScanForm::Update(unsigned steptime)
{
    exgr.RunStep(steptime);

    const bool *keystate = GetKeyboardState(nullptr);
    if( keystate[ASCII_ESC] )
        Close();

    if( keystate[KEY_UP] && curr_sel >= 1 )
        --curr_sel;

    if( keystate[KEY_DOWN] && curr_sel + 1 < infolist.size() )
        ++curr_sel;

    if( keystate[ASCII_CR] && curr_sel < infolist.size() )
    {
        if( PushTalkForm(infolist[curr_sel]) )
            Suspend();
    }
}

void ScanForm::Render()
{
    int screen_cols = getmaxx(stdscr);
    int screen_rows = getmaxy(stdscr);
    int count_row = 0;
    int label_row = 1;
    int separator_row = 2;
    int list_begin_row = 3;
    int list_rows = screen_rows - 3;

    mvwprintw(stdscr, count_row, 0, "Listed number: %zu", infolist.size());

    mvwprintw(
        stdscr,
        label_row,
        0,
        "%-8s%-40s%-16s%-8s%-8s",
        "Index",
        "Name",
        "Password",
        "Users",
        "Max");

    move(separator_row, 0);
    for(int i = 0; i < screen_cols; ++i)
        addch('-');

    if( infolist.size() )
    {
        unsigned begin_index =
            (int) curr_sel > list_rows / 2 ?
            curr_sel - list_rows / 2 :
            0;
        unsigned end_index =
            begin_index + list_rows < infolist.size() ?
            begin_index + list_rows :
            infolist.size();

        move(list_begin_row, 0);
        for(unsigned i = begin_index; i != end_index; ++i)
        {
            const ServerInfo &info = infolist[i];

            if( i == curr_sel )
                attron(A_REVERSE);

            string name =
                info.name.length() ?
                info.name :
                IPv4ToStr(info.addr.GetIP());

            wprintw(
                stdscr,
                "%-8d%-40s%-16s%-8u%-8u",
                i,
                name.c_str(),
                info.need_password ? "required" : "",
                info.curr_joined_num,
                info.max_joined_num);

            if( i == curr_sel )
                attroff(A_REVERSE);
        }
    }
}

void ScanForm::EventOnServerInfo(void *self, const ServerInfo &info)
{
    ((ScanForm*)self)->OnServerInfo(info);
}

void ScanForm::OnServerInfo(const ServerInfo &info)
{
    for(auto item : infolist)
    {
        if( item.addr.GetIP().val == info.addr.GetIP().val &&
            item.addr.GetPort() == info.addr.GetPort() )
        {
            return;
        }
    }

    infolist.push_back(info);
}
