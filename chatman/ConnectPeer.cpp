#include "ConnectPeer.h"

using namespace std;

ConnectPeer::ConnectPeer(
    SocketUdp *sock,
    void *eventhost,
    void(*OnQueryResp)(void*, const ServerInfo&),
    void(*OnJoinResp)(void*, int, int)) :
        CmdPeerBase(CMDSENDER_ANON, sock)
{
    query_proc = new QueryProcessor(this, eventhost, OnQueryResp);
    join_proc = new JoinProcessor(this, eventhost, OnJoinResp);

    AddProcessor(query_proc);
    AddProcessor(join_proc);
}

bool ConnectPeer::SendQueryCommand()
{
    return query_proc->SendCommand();
}

bool ConnectPeer::SendJoinCommand(
    const string &username,
    const string &password,
    const vector<uint8_t> &pubkey,
    const vector<uint8_t> &skey)
{
    return join_proc->SendCommand(username, password, pubkey, skey);
}
