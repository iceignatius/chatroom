#include "common/ascii.h"
#include "TalkForm.h"

using namespace std;
using namespace ui;

TalkForm::TalkForm(FormManager *mgr) :
    FormBase(mgr)
{
}

const char* TalkForm::KlassName()
{
    return "talkform";
}

Form* TalkForm::CreateInstance(FormManager *mgr)
{
    return new TalkForm(mgr);
}

void TalkForm::OnPush(const FormParams &params)
{
    SocketAddr addr = params.GetNetAddrParam("addr", SocketAddr());
    string username = params.GetStringParam("username", "");
    string password = params.GetStringParam("password", "");

    if( addr.IsAvailable() )
        client.Connect(addr, username, password);
}

void TalkForm::OnPop(FormParams &params)
{
    client.Disconnect();
}

void TalkForm::ParseTextInput(const bool keystate[])
{
    if( client.GetState() != ChatMan::State::Talk )
        return;

    textinput.PushKeyState(keystate);

    if( textinput.HaveCompletedText() )
        client.SendText(textinput.PopCompletedText());
}

void TalkForm::ReceiveRemoteText()
{
    string text, sender;
    if( !client.ReceiveText(text, sender) )
        return;

    string line = sender + ": " + text;
    msglist.push_back(line);
}

void TalkForm::RenderTalk()
{
    int screen_cols = getmaxx(stdscr);
    int screen_rows = getmaxy(stdscr);
    int buffered_row = screen_rows - 1;
    int separator_row = buffered_row - 1;
    int list_rows = screen_rows - 2;

    while( (int) msglist.size() > list_rows )
        msglist.pop_front();

    move(0, 0);
    for(auto text : msglist)
        wprintw(stdscr, "%s\n\r", text.c_str());

    move(separator_row, 0);
    for(int i = 0; i < screen_cols; ++i)
        addch('-');

    if( textinput.HaveBufferedText() )
        mvwprintw(stdscr, buffered_row, 0, "%s", textinput.GetBufferedText().c_str());
}

void TalkForm::RenderConnectingStatus()
{
    string msg;
    if( client.GetState(&msg) != ChatMan::State::Talk )
        Manager()->GetConsole()->RenderMessageBox(msg);
}

void TalkForm::Update(unsigned steptime)
{
    const bool *keystate = GetKeyboardState(nullptr);
    if( keystate[ASCII_ESC] )
        Close();

    ParseTextInput(keystate);
    client.Update(steptime);
    ReceiveRemoteText();

    string errtext;
    auto state = client.GetState(&errtext);
    if( state == ChatMan::State::Disconnected || state == ChatMan::State::Error )
    {
        FormParams params;
        params.InsertStringParam("text", errtext);
        params.InsertUintParam("timeout", 3*1000);
        Manager()->PushNewForm("messageform", params);

        Close();
    }
}

void TalkForm::Render()
{
    RenderTalk();
    RenderConnectingStatus();
}
