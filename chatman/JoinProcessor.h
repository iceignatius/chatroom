#ifndef _JOIN_PROCESSOR_H_
#define _JOIN_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class JoinProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnJoin)(void *host, int errcode, int userid);

    std::vector<uint8_t> pubkey;

public:
    JoinProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnJoin)(void*, int, int));

private:
    void OnResponseMessage(
        const net::CmdMsg &msg,
        const SocketAddr &srcaddr) override;

public:
    bool SendCommand(
        const std::string &username,
        const std::string &password,
        const std::vector<uint8_t> &pubkey,
        const std::vector<uint8_t> &skey);
};

#endif
