#ifndef _TALK_FORM_H_
#define _TALK_FORM_H_

#include "ui/FormBase.h"
#include "TextInput.h"
#include "ChatMan.h"

class TalkForm : public ui::FormBase
{
private:
    std::list<std::string> msglist;
    TextInput textinput;

    ChatMan client;

private:
    TalkForm(ui::FormManager *mgr);

public:
    static const char* KlassName();
    static Form* CreateInstance(ui::FormManager *mgr);

private:
    void OnPush(const ui::FormParams &params) override;
    void OnPop(ui::FormParams &params) override;

private:
    void ParseTextInput(const bool keystate[]);
    void ReceiveRemoteText();

    void RenderTalk();
    void RenderConnectingStatus();

public:
    void Update(unsigned steptime) override;
    void Render() override;
};

#endif
