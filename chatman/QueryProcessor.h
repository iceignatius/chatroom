#ifndef _QUERY_PROCESSOR_H_
#define _QUERY_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

struct ServerInfo
{
    SocketAddr addr;
    std::string name;
    unsigned curr_joined_num;
    unsigned max_joined_num;
    bool need_password;
    std::vector<uint8_t> pubkey;
};

class QueryProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnQuery)(void *host, const ServerInfo &info);

public:
    QueryProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnQuery)(void*, const ServerInfo&));

private:
    void OnResponseMessage(
        const net::CmdMsg &msg,
        const SocketAddr &srcaddr) override;

public:
    bool SendCommand();
    bool SendCommand(const SocketAddr &addr);
};

#endif
