#include "net/cmdcrypt.h"
#include "JoinProcessor.h"

using namespace std;
using namespace net;

JoinProcessor::JoinProcessor(
    CmdPeer *peer,
    void *eventhost,
    void(*OnJoin)(void*, int, int)) :
        CmdProcessorBase(CMDID_JOIN, 0, peer)
{
    this->eventhost = eventhost;
    this->OnJoin = OnJoin;
}

void JoinProcessor::OnResponseMessage(
    const CmdMsg &msg,
    const SocketAddr &srcaddr)
{
    int errcode = msg.GetErrorCode();
    int userid = msg.GetIntegerArg("client-id", -1);

    vector<uint8_t> signature = msg.GetBinaryArg("signature");
    if( pubkey.size() && !signature.size() )
        return;

    if( !CmdCrypt::EccVerifyJoinRes(signature, errcode, userid, pubkey) )
        return;

    OnJoin(eventhost, errcode, userid);
}

bool JoinProcessor::SendCommand(
    const string &username,
    const string &password,
    const vector<uint8_t> &pubkey,
    const vector<uint8_t> &skey)
{
    CmdMsg msg(CmdId());

    msg.AddStringArg("name", username);

    this->pubkey.clear();
    if( pubkey.size() && skey.size() )
    {
        vector<uint8_t> encrypted_skey =
            CmdCrypt::EccEncrypt(skey, pubkey);
        if( !encrypted_skey.size() ) return false;

        vector<uint8_t> passphrase =
            CmdCrypt::CalculatePassphrase(password, skey);
        if( !passphrase.size() ) return false;

        msg.AddBinaryArg("session-key", encrypted_skey);
        msg.AddBinaryArg("passphrase", passphrase);

        this->pubkey = pubkey;
    }

    return SendRequestMessage(msg);
}
