#ifndef _SCAN_PEER_H_
#define _SCAN_PEER_H_

#include "net/CmdPeerBase.h"
#include "QueryProcessor.h"

class ScanPeer : public net::CmdPeerBase
{
private:
    SocketUdp *sock;
    QueryProcessor *query_proc;

public:
    ScanPeer(
        SocketUdp *sock,
        void *eventhost,
        void(*OnServerInfo)(void*, const ServerInfo&));

public:
    void SendQuery();
};

#endif
