#ifndef _QUIT_PROCESSOR_H_
#define _QUIT_PROCESSOR_H_

#include "net/CmdProcessorBase.h"

class QuitProcessor : public net::CmdProcessorBase
{
private:
    void *eventhost;
    void(*OnQuit)(void *host);

public:
    QuitProcessor(
        net::CmdPeer *peer,
        void *eventhost,
        void(*OnQuit)(void*));

private:
    void OnResponseMessage(
        const net::CmdMsg &msg,
        const SocketAddr &srcaddr) override;

public:
    bool SendCommand();
};

#endif
