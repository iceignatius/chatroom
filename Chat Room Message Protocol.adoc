= Char Room Message Protocol =

本文件描述有關聊天室程式的網路訊息交換相關協議約定與命令。

* 本文件中所記載的所有資料大小、位移位置等，皆以 8 位元位元組為單位(octets)。
* 本文件中所記載的所有整數皆使用 big-endian 格式。
* 由於聊天室終端與伺服器都會主動向對方發送訊息，
    因此在本文件的描述中比較少使用終端或伺服器等用語，
    而以「命令端」與「回應端」稱呼。


== Packet ==

一個資料封包由數個欄位組成，每一個欄位按照順序前後連接，中間沒有任何空白。
封包的格式表示如下：

.Packet format
[width="80%", cols="^1,^1,4", options="header"]
|=======================================
| Size | Type | Name
| 8 | Uint | <<signature>>
| 2 | Int | <<sender>>
| 2 | Uint | <<flags>>
| 8 | Uint | <<command-id>>
| 2 | Uint | <<command-sn>>
| 2 | Uint | <<argslen>>
| Variable | Binary | <<argsdata>>
| 16 | Binary | <<macdata>>
| 4 | Uint | <<checksum>>
|=======================================

[[signature]]
=== Signature ===

A magic number, always be 0x3EBAF03FA11957B6.

[[sender]]
=== Sender ID ===

標示訊息發送者的一個識別數字，其中：

* Positive value: 表示訊息由聊天室終端所發出。
    該數值為終端使用者加入伺服器之時，由伺服器所發派給終端的一個識別號。
* Negative value: 表示無效使用者、或尚未加入聊天室的匿名使用者。
* Zero: 表示訊息由伺服器所發出。

[[flags]]
=== Flags ===

表示與封包資料和收送行為有關的屬性旗標，其值可為零至數個以下所列數值的 bit or 集合：

* `NO_RESP` (0x1): 表示該封包為單向傳送的封包，不需要回應訊息。
    通常是伺服器的週期性資料更新等命令會設置這個旗標。

* `NO_MAC` (0x2): 表示該封包未具有有效的 MAC 認証資料。
    在聊天室終端成功加入伺服器之前的雙方往返封包，可以設置這個旗標。

    若在某些要求必須進行 MAC 驗證的命令封包中未設置這個旗標，
    則該請求可能會被忽略、拒絕、或者接受等，由回應端決定。

[[command-id]]
=== Command Identifier ===

表示本封包所要傳達的主要命令之命令代號。
有效的命令代號為除零以外的任何數字，而命令代號與具體命令的對應關係由不同的應用決定，
本文後部會列舉聊天室應用所定義的基本命令。

有效的命令代號範圍雖廣，但為了代號定義、避免代號衝突、以及開發除錯上的原因，
建議代號的實際資料內容，應為可讀字元的組成。
在此原則下，建議命令代號的每個位元應由 ASCII Printable 範圍內的字元組成，
並左靠對齊，後部剩餘空間則以零值補滿。

若該命令所對應的工作需要傳遞更多參數資料才能完成，則所需的其他資料應置於 <<argsdata>> 欄位。
本聊天室所支援的命令列表，以及其所附帶的資料參數，請參閱 <<commands>> 章節。

[[command-sn]]
=== Command Serial Number ===

此序號為一個循環累加的數字，表示命令封包的序號，
用來核對所接收命令封包的順序，以及識別對應的訊息回覆封包。

該序號記錄的意義為命令的序號，而非封包的序號。
應此，只有同一個通訊連接中的同一個命令處理程序兩端，
擁有獨立的命令序號系統，需要維護並驗證命令序號。

序號由訊息發起的命令端指派，
在終端使用者加入聊天室伺服器時，雙方各命令的通訊封包序號皆應初始為零。

命令端的每一個獨立的命令請求封包的序號，都應是上一個請求封包的序號加二，
若新值溢出時則截斷溢出的部份；
若該請求是因為未收到有效的回覆因而重送的封包，則該封包的序號需與原封包相同、不累加。

回應端回應的訊息封包序號應為對應的請求封包之序號加一。
由於序號在命令端每次都加二，而在回應端的序號皆為對應封包序號加一，
因此命令端的序號總為偶數，而回應的序號則總為奇數。

對於使用者在成功加入伺服器之前的封包，如探詢命令封包等，其之序號應永遠為零。

[[argslen]]
=== Arguments Length ===

此為一個整數，記錄 <<argsdata>> 資料的長度；若無資料，則本值為零。

[[argsdata]]
=== Arguments Data ===

當個別的命令需要攜帶更多的資料才能夠完成命令所需的工作時，
額外的資料群將安置於這個欄位下。

本欄位的資料格式採用
link:https://gitlab.com/iceignatius/ndobj[Named Data Object] 格式，
根節點為一個名稱為空字串的 MAP 型態節點，
個別命令所需要的其他資料將安放於該根節點之下。

[[macdata]]
=== MAC Data ===

為使用通信兩端所約定使用的 session key，以 AES 加密演算法執行 CBC-MAC 計算，
所得的 128-bits MAC 認証碼資料。
訊息認証的計算資料範圍為從 <<signature>> 的第一個位元組
至 <<argsdata>> 的最後一個位元組之間的所有資料。

在 `HAVE_MAC` 旗標設置的情況下，本欄位資料有效；
其餘情況下本欄位資料會被忽略，在此情況下建議將本欄位資料以零值填充。

[[checksum]]
=== Checksum ===

一個 CRC-32 校驗數字，
計算封包資料從 <<signature>> 的第一個位元組
至 <<macdata>> 的最後一個位元組之間所有資料的 CRC-32 校驗值。


[[exchange]]
== Message Exchange ==

=== 命令端發送命令 ===

1. 命令端將產製的命令封包發送至對方，並開始等待接收回應封包。
    若命令封包設置了 `NO_RESP` (<<flags>>) 旗標，則無需等待回應封包，亦無需執行後續動作。

2. 若等待了 `RESEND_TIMEOUT` 時間後無法收到有效的回應封包，
    則應再次重送命令封包，並記錄重送次數為 1。

3. 若等待了 `RESEND_TIMEOUT` 時間後仍無法收到有效的回應封包，
    則應再次重送命令封包，並將重送次數記錄加 1。

4. 若等待了 `RESEND_TIMEOUT` 時間後仍無法收到有效的回應封包，並且重送次數已達 `RESEND_TRYCOUNT`，
    則應停止命令收送流程，中斷連線，並以通訊錯誤之訊息通知其他相關模組進行相應程序。

5. 若在等待時間內收到了有效的回應封包，
    亦即封包校驗成功、回應之命令碼與所送出的相同、並且命令序號為所送出之序號加 1，
    則應停止命令收送流程，並解析封包內容以執行對應行為。

=== 回應端接受命令 ===

1. 回應端接收到一個有效的命令封包。

2. 檢查該命令封包是否為已經接收過的重複封包，
    若是，則直接取用並重送先前的回應封包備份，並略過後續流程。

3. 檢查封包傳送者 <<sender>> 是否為可接受的，
    若否，則應產生並發送錯誤訊息為 `INVALID_USER` 的回應封包，
    並略過後續流程。

4. 檢查是否支援命令封包的命令碼 <<command-id>>，
    若不支援該命令，則應產生並發送錯誤訊息為 `UNSUPPORTED_COMMAND` 的回應封包，
    並略過後續流程。

5. 檢查命令序號 <<command-sn>> 是否在可接受範圍，若否，則丟棄該封包並略過後續流程：
    * 若為需要回應封包的命令，則所收到的命令封包之命令序號，應為上一次收到的命令序號加 2；
        若無上一次的命令，則命令序號應為 0。
    * 若為無需回應封包的命令，則所收到的命令序號應新於上一次收到的命令序號，
        並且其差值未超出序號值域範圍的一半。

6. 執行該命令所對應的工作內容，並產生回應封包。

7. 發送回應封包，同時將該回應封包備份一段時間，好在收到重複的命令封包時可以直接回覆之。

=== 維持連線 ===

建立連線的兩端必須經常發送訊息給對方，
若一段時間內可能因為空閒的緣故導致沒有任何訊息可送，則應發送 <<alive>> 命令。
若通訊的一端在過去的 `ALIVE_CHECK_PERIOD` 時間內沒有接收到任何有效的命令，
則視為對方已無回應，應中斷連線並釋放連線資源。


[[commands]]
== Commands ==

聊天室所需要支援的基本命令有：<<query>>、<<join>>、<<quit>>、<<alive>>、以及 <<talk>>。
個別命令對應的行為、以及所需攜帶的資料等將於後文詳述。

[[common-fields]]
=== Common Fields ===

大部份的回應封包都會攜帶的兩個欄位資料如下：

* Error Code:
    為一個無號整數，在命令發生錯誤或被拒絕等情況發生時，用以表達錯誤的原因，
    其中零值代表命令執行成功。
    其他基本錯誤碼的定義請參閱 <<error-code-def>> 章節。

* Error Text:
    為 error code 所對應的文字訊息，若命令執行成功，則該訊息內容為空。 +
    +
    理論上只需要 error code 就可以查找對應的文字描述，
    但因為程式更新以及用戶端支援程度的不同，用戶端不見得能夠查找完整的錯誤訊息列表，
    因此由伺服器一併攜帶錯誤描述訊息，以利用戶端分析除錯。

[[query]]
=== Query ===

探尋伺服器的存在，或索取伺服器連線資訊與參數。

* Command ID: 0x7175657279000000
* Flags: NO_MAC (<<flags>>)

.Query request arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
|=======================================

.Query response arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
| name | String | Optional | Name of the chat room
| curr-joined-num | Uint | Mandatory | Current number of joined users
| max-joined-num | Uint | Mandatory | Maximum number of supported users
| need-password | Boolean | Optional | <<password-flag>>
| public-key | Binary | Conditional (if authenticated communication enabled) | <<public-key>>
|=======================================

[[password-flag]]
=== Password Flag ===

若伺服器要求使用密碼登入，則必須攜帶此欄位，並且該值須為 TRUE。
若該欄位值為 FALSE、或未攜帶該欄位，則表示伺服器不需使用密碼登入。

[[public-key]]
=== Public Key ===

伺服器若要求有身份認証的通訊，則必須提供伺服器的公鑰，該金鑰為 ECC 加密演算法的公鑰資料；
若伺服器不要求通訊認証，則不提供公鑰即可。

若伺服器要求使用密碼登入，則必須要求身份認証的通訊，並提供公鑰。

[[join]]
=== Join ===

加入聊天伺服器。

* Command ID: 0x6A6F696E00000000
* Flags: NO_MAC (<<flags>>)

.Join request arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
| name | String | Mandatory | The human readable client user name
| session-key | Binary | Conditional (if authenticated communication enabled) | <<encrypted-session-key>>
| passphrase | Binary | Conditional (if authenticated communication enabled) | <<passphrase>>
|=======================================

.Join response arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
| error-code | Uint | Mandatory | Error code in <<common-fields>>
| error-text | String | Mandatory | Error text in <<common-fields>>
| client-id | Int | Conditional (if succeed) | The server assigned client ID
| signature | Binary | Conditional (if authenticated communication enabled) | <<join-signature>>
|=======================================

[[encrypted-session-key]]
==== Encrypted Session Key ====

這是一個使用伺服器公鑰(從 <<query>> 命令的回應中取得)加密後的通訊密鑰，
若伺服器未要求使用加密通訊的話，則可不攜帶通訊密鑰資料。

通訊密鑰由客戶端產生，並在加入伺服器命令中傳送給伺服器。
在客戶端成功加入伺服器後，直到退出伺服器之前，
兩方往返的所有封包都需要使用該密鑰計算 <<macdata>> 以進行簽章認証。

[[passphrase]]
==== Passphrase ====

這是使用密碼登錄伺服器時所需產生的密碼簽章資料，
該資料即為登入密碼明碼使用 session key 所計算出來的 MAC 資料；
若不使用密碼登入，則以 8 個空白字符組成的字串代替登入密碼的位置，計算 MAC 值。

[[join-signature]]
==== Signature ====

由於 Join 命令往返的過程中，封包認証的機制尚未完全建立，
因此需要額外的手段來驗證封包發送者，此欄位即作為驗證伺服器回應訊息所用之簽章。

產生簽章的方法為：
先產生 8 個位元組的「關鍵資料」，再用伺服器私鑰對該「關鍵資料」計算簽章，
該簽章即為所需之簽章資料。
而計算簽章所需的「關鍵資料」由兩個部份組成：
前 4 個位元組為回應訊息中的錯誤碼(<<common-fields>>)整數，
後 4 個位元組為回應訊息中的 client ID 整數，若無則以零代，
兩個整數皆為 32-bits、unsigned、big-endian 格式。

[[quit]]
=== Quit ===

退出聊天伺服器。

對於伺服器的實做而言，客戶端退出聊天室的命令是一種通知的作用，理論上不存在退出失敗的情況，
甚至用戶端可能不會等待回應訊息就會直接斷線。
然而為了更好的相容用戶端的實做，仍建議應在回應訊息中攜帶錯誤碼與錯預訊息欄位。
此外，伺服器不應只依賴客戶端發出退出的命令來退出用戶，
應同時搭配其他檢測用戶是否在線的機制，以妥善維護有效的伺服器資源。

* Command ID: 0x7175697400000000
* Flags: None

.Quit request arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
|=======================================

.Quit response arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
| error-code | Uint | Optional | Error code in <<common-fields>>
| error-text | String | Optional | Error text in <<common-fields>>
|=======================================

[[alive]]
=== Alive ===

定期發送以保持連線狀態。

在通訊兩端連線建立後的任何時間裡，若在過去的 `ALIVE_SEND_PERIOD` 時間內沒有發送出任何訊息，
則應發送此命令，以維繫連線狀態。

* Command ID: 0x616C697665000000
* Flags: `NO_RESP` (<<flags>>)

.Alive request arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
|=======================================

[[talk]]
=== Talk ===

發送聊天訊息。

用戶端發出此命令以發送聊天訊息；
而伺服端發送此命令以將聊天訊息通知給所有用戶。

* Command ID: 0x74616C6B00000000
* Flags: None

.Talk request arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
| sender | String | Conditional (if send from server) | Name of the client who send the message
| text | String | Mandatory | The chat message
|=======================================

.Talk response arguments
[options="header"]
|=======================================
| Name | Type | Present | Description
| error-code | Uint | Mandatory | Error code in <<common-fields>>
| error-text | String | Mandatory | Error text in <<common-fields>>
|=======================================


== Appendix ==

[[error-code-def]]
=== Error Code Definition ===

_Contents not implemented yet!_

[[parameters]]
=== Parameters ===

* `RESEND_TIMEOUT`: 命令封包超時重送的等待時間，建議預設為 2 秒。
* `RESEND_TRYCOUNT`: 命令封包最大重送次數，建議預設為 3 次。
* `ALIVE_CHECK_PERIOD`: 判斷連線無回應的檢查時間，建議預設為 30 秒。
* `ALIVE_SEND_PERIOD`: 判斷傳輸閒置而主動發送 alive 命令的檢查時間，建議預設為 5 秒。
